The Drupal Association engineering team maintains a number of modules that
are used exclusively to support Drupal.org and the custom workflows involved
with maintaining an open source community.

This projects include:
https://www.drupal.org/project/drupalorg
https://www.drupal.org/project/infrastructure
https://www.drupal.org/project/gitlab_templates

As well as a number of private repositories where appropriate (secrets)

These modules provide customizations used on Drupal.org itself. Other sites
SHOULD NOT INSTALL OR USE THESE MODULES! They are only included in the main
Drupal contributions repository for three reasons:

  1) To facilitate members of the Drupal community who wish to help
     debug problems and improve functionality on Drupal.org.

  2) Certain Drupal.org-specific assumptions are made in other modules
     (in particular, the project and project_issue suite of modules), and
     some code should be moved from those modules into here. Having a
     public issue queue for the drupalorg.module will help coordinate these
     development efforts.

  3) The educational value of an example of the kinds of customizations
     you can do via a site-specific module.

Tagged releases will never be made for this module, since we update Drupal.org
based on our changes in Git. We have no intention of supporting this module
such that other sites can make use of it.

p.s. Did we mention you don’t want to run this module on your own site?!

## Contributing

For non-trivial changes, be sure that the approach is vetted in the issue queue
before spending too much time on implementation. Use issue forks to work from
the `7.x-3.x` branch.
