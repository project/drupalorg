<?php

/**
 * @file
 * drupalorg_wysiwyg.features.ckeditor_profile.inc
 */

/**
 * Implements hook_ckeditor_profile_defaults().
 */
function drupalorg_wysiwyg_ckeditor_profile_defaults() {
  $data = array(
    'CKEditor Global Profile' => array(
      'name' => 'CKEditor Global Profile',
      'settings' => array(
        'skin' => 'moono-lisa',
        'ckeditor_path' => '%l/ckeditor',
        'ckeditor_local_path' => '',
        'ckeditor_plugins_path' => '%l/ckeditor/plugins',
        'ckeditor_plugins_local_path' => '',
        'ckfinder_path' => '%m/ckfinder',
        'ckfinder_local_path' => '',
        'ckeditor_aggregate' => 'f',
        'toolbar_wizard' => 'f',
        'loadPlugins' => array(),
      ),
      'input_formats' => array(),
    ),
    'Filtered' => array(
      'name' => 'Filtered',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
  [\'Bold\',\'Italic\',\'Strike\',\'Code\',\'Format\'],
  [\'NumberedList\',\'BulletedList\',\'-\',\'Blockquote\'],
  [\'Link\',\'Unlink\',\'Anchor\'],
  [\'Templates\',\'CodeSnippet\',\'Image\',\'Table\',\'HorizontalRule\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => '#9498C6',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => 'dl;dd;dt;*[id];small;div(note,note-tip,note-warning,note-version,big-stats,nudge);blockquote(big);cite;abbr[title];th[colspan,rowspan];td[colspan,rowspan];details;summary',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;h2;h3;h4;h5;h6;pre',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'theme',
        'css_path' => '',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'disableNativeSpellChecker' => 'f',
        'config_js' => 'theme',
        'config_js_path' => '',
        'js_conf' => '',
        'loadPlugins' => array(
          'blockquote' => array(
            'name' => 'blockquote',
            'desc' => 'Plugin file: blockquote',
            'path' => '%plugin_dir_extra%blockquote/',
            'buttons' => array(
              'Blockquote' => array(
                'label' => 'Blockquote',
                'icon' => 'icons/blockquote.png',
              ),
            ),
            'default' => 'f',
          ),
          'button' => array(
            'name' => 'button',
            'desc' => 'Plugin file: button',
            'path' => '%plugin_dir_extra%button/',
            'buttons' => array(
              'MyBold' => array(
                'label' => 'MyBold',
                'icon' => 'icons/mybold.png',
              ),
              'my_button' => array(
                'label' => 'my_button',
                'icon' => 'icons/my_button.png',
              ),
            ),
            'default' => 'f',
          ),
          'codesnippet' => array(
            'name' => 'codesnippet',
            'desc' => 'Plugin file: codesnippet',
            'path' => '%plugin_dir_extra%codesnippet/',
            'buttons' => array(
              'CodeSnippet' => array(
                'label' => 'CodeSnippet',
                'icon' => 'icons/codesnippet.png',
              ),
            ),
            'default' => 'f',
          ),
          'codestyle' => array(
            'name' => 'codestyle',
            'desc' => 'Plugin file: codestyle',
            'path' => '%plugin_dir_extra%codestyle/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'counter' => array(
            'name' => 'counter',
            'desc' => 'Plugin to count symbols, symbols without blanks and words',
            'path' => '%plugin_dir%counter/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
          'filetools' => array(
            'name' => 'filetools',
            'desc' => 'Plugin file: filetools',
            'path' => '%plugin_dir_extra%filetools/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'format' => array(
            'name' => 'format',
            'desc' => 'Plugin file: format',
            'path' => '%plugin_dir_extra%format/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'horizontalrule' => array(
            'name' => 'horizontalrule',
            'desc' => 'Plugin file: horizontalrule',
            'path' => '%plugin_dir_extra%horizontalrule/',
            'buttons' => array(
              'HorizontalRule' => array(
                'label' => 'HorizontalRule',
                'icon' => 'icons/horizontalrule.png',
              ),
            ),
            'default' => 'f',
          ),
          'htmlwriter' => array(
            'name' => 'htmlwriter',
            'desc' => 'Plugin file: htmlwriter',
            'path' => '%plugin_dir_extra%htmlwriter/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'image2' => array(
            'name' => 'image2',
            'desc' => 'Plugin file: image2',
            'path' => '%plugin_dir_extra%image2/',
            'buttons' => array(
              'Image' => array(
                'label' => 'Image',
                'icon' => 'icons/image.png',
              ),
            ),
            'default' => 'f',
          ),
          'lineutils' => array(
            'name' => 'lineutils',
            'desc' => 'Plugin file: lineutils',
            'path' => '%plugin_dir_extra%lineutils/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'magicline' => array(
            'name' => 'magicline',
            'desc' => 'Plugin file: magicline',
            'path' => '%plugin_dir_extra%magicline/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'mediaembed' => array(
            'name' => 'mediaembed',
            'desc' => 'Plugin for embedding HTML snippets',
            'path' => '%plugin_dir%mediaembed/',
            'buttons' => array(
              'MediaEmbed' => array(
                'label' => 'MediaEmbed',
                'icon' => 'images/icon.png',
              ),
            ),
            'default' => 'f',
          ),
          'notification' => array(
            'name' => 'notification',
            'desc' => 'Plugin file: notification',
            'path' => '%plugin_dir_extra%notification/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'prism' => array(
            'name' => 'prism',
            'desc' => 'Plugin file: prism',
            'path' => '%plugin_dir_extra%prism/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'resize' => array(
            'name' => 'resize',
            'desc' => 'Plugin file: resize',
            'path' => '%plugin_dir_extra%resize/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'table' => array(
            'name' => 'table',
            'desc' => 'Plugin file: table',
            'path' => '%plugin_dir_extra%table/',
            'buttons' => array(
              'Table' => array(
                'label' => 'Table',
                'icon' => 'icons/table.png',
              ),
            ),
            'default' => 'f',
          ),
          'templates' => array(
            'name' => 'templates',
            'desc' => 'Plugin file: templates',
            'path' => '%plugin_dir_extra%templates/',
            'buttons' => array(
              'Templates' => array(
                'label' => 'Templates',
                'icon' => 'icons/templates.png',
              ),
            ),
            'default' => 'f',
          ),
          'toolbar' => array(
            'name' => 'toolbar',
            'desc' => 'Plugin file: toolbar',
            'path' => '%plugin_dir_extra%toolbar/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'widget' => array(
            'name' => 'widget',
            'desc' => 'Plugin file: widget',
            'path' => '%plugin_dir_extra%widget/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
        ),
      ),
      'input_formats' => array(
        1 => 'Filtered HTML',
      ),
    ),
    'Full' => array(
      'name' => 'Full',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
  [\'Bold\',\'Italic\',\'Strike\',\'Code\',\'Format\'],
  [\'NumberedList\',\'BulletedList\',\'-\',\'Blockquote\'],
  [\'Link\',\'Unlink\',\'Anchor\'],
  [\'Templates\',\'CodeSnippet\',\'Image\',\'Table\',\'HorizontalRule\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => '#9498C6',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => 'dl;dd;dt;*[id];iframe[*];div[*];*(*);cite;abbr[title];th[colspan,rowspan];td[colspan,rowspan];details;summary',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;h2;h3;h4;h5;h6;pre',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'theme',
        'css_path' => '',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'disableNativeSpellChecker' => 'f',
        'config_js' => 'theme',
        'config_js_path' => '',
        'js_conf' => '',
        'loadPlugins' => array(
          'blockquote' => array(
            'name' => 'blockquote',
            'desc' => 'Plugin file: blockquote',
            'path' => '%plugin_dir_extra%blockquote/',
            'buttons' => array(
              'Blockquote' => array(
                'label' => 'Blockquote',
                'icon' => 'icons/blockquote.png',
              ),
            ),
            'default' => 'f',
          ),
          'button' => array(
            'name' => 'button',
            'desc' => 'Plugin file: button',
            'path' => '%plugin_dir_extra%button/',
            'buttons' => array(
              'MyBold' => array(
                'label' => 'MyBold',
                'icon' => 'icons/mybold.png',
              ),
              'my_button' => array(
                'label' => 'my_button',
                'icon' => 'icons/my_button.png',
              ),
            ),
            'default' => 'f',
          ),
          'codesnippet' => array(
            'name' => 'codesnippet',
            'desc' => 'Plugin file: codesnippet',
            'path' => '%plugin_dir_extra%codesnippet/',
            'buttons' => array(
              'CodeSnippet' => array(
                'label' => 'CodeSnippet',
                'icon' => 'icons/codesnippet.png',
              ),
            ),
            'default' => 'f',
          ),
          'codestyle' => array(
            'name' => 'codestyle',
            'desc' => 'Plugin file: codestyle',
            'path' => '%plugin_dir_extra%codestyle/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'counter' => array(
            'name' => 'counter',
            'desc' => 'Plugin to count symbols, symbols without blanks and words',
            'path' => '%plugin_dir%counter/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
          'filetools' => array(
            'name' => 'filetools',
            'desc' => 'Plugin file: filetools',
            'path' => '%plugin_dir_extra%filetools/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'format' => array(
            'name' => 'format',
            'desc' => 'Plugin file: format',
            'path' => '%plugin_dir_extra%format/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'horizontalrule' => array(
            'name' => 'horizontalrule',
            'desc' => 'Plugin file: horizontalrule',
            'path' => '%plugin_dir_extra%horizontalrule/',
            'buttons' => array(
              'HorizontalRule' => array(
                'label' => 'HorizontalRule',
                'icon' => 'icons/horizontalrule.png',
              ),
            ),
            'default' => 'f',
          ),
          'htmlwriter' => array(
            'name' => 'htmlwriter',
            'desc' => 'Plugin file: htmlwriter',
            'path' => '%plugin_dir_extra%htmlwriter/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'image2' => array(
            'name' => 'image2',
            'desc' => 'Plugin file: image2',
            'path' => '%plugin_dir_extra%image2/',
            'buttons' => array(
              'Image' => array(
                'label' => 'Image',
                'icon' => 'icons/image.png',
              ),
            ),
            'default' => 'f',
          ),
          'lineutils' => array(
            'name' => 'lineutils',
            'desc' => 'Plugin file: lineutils',
            'path' => '%plugin_dir_extra%lineutils/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'magicline' => array(
            'name' => 'magicline',
            'desc' => 'Plugin file: magicline',
            'path' => '%plugin_dir_extra%magicline/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'mediaembed' => array(
            'name' => 'mediaembed',
            'desc' => 'Plugin for embedding HTML snippets',
            'path' => '%plugin_dir%mediaembed/',
            'buttons' => array(
              'MediaEmbed' => array(
                'label' => 'MediaEmbed',
                'icon' => 'images/icon.png',
              ),
            ),
            'default' => 'f',
          ),
          'notification' => array(
            'name' => 'notification',
            'desc' => 'Plugin file: notification',
            'path' => '%plugin_dir_extra%notification/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'notificationaggregator' => array(
            'name' => 'notificationaggregator',
            'desc' => 'Plugin file: notificationaggregator',
            'path' => '%plugin_dir_extra%notificationaggregator/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'prism' => array(
            'name' => 'prism',
            'desc' => 'Plugin file: prism',
            'path' => '%plugin_dir_extra%prism/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'resize' => array(
            'name' => 'resize',
            'desc' => 'Plugin file: resize',
            'path' => '%plugin_dir_extra%resize/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'table' => array(
            'name' => 'table',
            'desc' => 'Plugin file: table',
            'path' => '%plugin_dir_extra%table/',
            'buttons' => array(
              'Table' => array(
                'label' => 'Table',
                'icon' => 'icons/table.png',
              ),
            ),
            'default' => 'f',
          ),
          'templates' => array(
            'name' => 'templates',
            'desc' => 'Plugin file: templates',
            'path' => '%plugin_dir_extra%templates/',
            'buttons' => array(
              'Templates' => array(
                'label' => 'Templates',
                'icon' => 'icons/templates.png',
              ),
            ),
            'default' => 'f',
          ),
          'toolbar' => array(
            'name' => 'toolbar',
            'desc' => 'Plugin file: toolbar',
            'path' => '%plugin_dir_extra%toolbar/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'widget' => array(
            'name' => 'widget',
            'desc' => 'Plugin file: widget',
            'path' => '%plugin_dir_extra%widget/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
        ),
      ),
      'input_formats' => array(
        3 => 'Full HTML',
      ),
    ),
  );
  return $data;
}
