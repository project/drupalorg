<?php

/**
 * @file
 * drupalorg_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function drupalorg_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Documentation moderator.
  $roles['Documentation moderator'] = array(
    'name' => 'Documentation moderator',
    'weight' => 7,
  );

  // Exported role: Git administrator.
  $roles['Git administrator'] = array(
    'name' => 'Git administrator',
    'weight' => 10,
  );

  // Exported role: Git user.
  $roles['Git user'] = array(
    'name' => 'Git user',
    'weight' => 11,
  );

  // Exported role: Git vetted user.
  $roles['Git vetted user'] = array(
    'name' => 'Git vetted user',
    'weight' => 12,
  );

  // Exported role: Section editor.
  $roles['Section editor'] = array(
    'name' => 'Section editor',
    'weight' => 20,
  );

  // Exported role: Site moderator.
  $roles['Site moderator'] = array(
    'name' => 'Site moderator',
    'weight' => 18,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 0,
  );

  // Exported role: association staff.
  $roles['association staff'] = array(
    'name' => 'association staff',
    'weight' => 19,
  );

  // Exported role: community.
  $roles['community'] = array(
    'name' => 'community',
    'weight' => 5,
  );

  // Exported role: confirmed.
  $roles['confirmed'] = array(
    'name' => 'confirmed',
    'weight' => 4,
  );

  // Exported role: content administrator.
  $roles['content administrator'] = array(
    'name' => 'content administrator',
    'weight' => 14,
  );

  // Exported role: content moderator.
  $roles['content moderator'] = array(
    'name' => 'content moderator',
    'weight' => 6,
  );

  // Exported role: security team.
  $roles['security team'] = array(
    'name' => 'security team',
    'weight' => 15,
  );

  // Exported role: user administrator.
  $roles['user administrator'] = array(
    'name' => 'user administrator',
    'weight' => 17,
  );

  return $roles;
}
