<?php

/**
 * @file
 * drupalorg_case_studies.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function drupalorg_case_studies_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_casestudy_technical|node|casestudy|default';
  $field_group->group_name = 'group_casestudy_technical';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'casestudy';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_modules';
  $field_group->data = array(
    'label' => 'Technical specifications inner',
    'weight' => '0',
    'children' => array(
      0 => 'field_module_selection',
      1 => 'taxonomy_vocabulary_5',
      2 => 'field_projects',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Technical specifications inner',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'accordion-content',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_casestudy_technical|node|casestudy|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_modules|node|casestudy|default';
  $field_group->group_name = 'group_modules';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'casestudy';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Technical specifications',
    'weight' => '5',
    'children' => array(
      0 => 'group_casestudy_technical',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Technical specifications',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'accordion',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_modules|node|casestudy|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_modules|node|casestudy|form';
  $field_group->group_name = 'group_modules';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'casestudy';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Technical specifications',
    'weight' => '8',
    'children' => array(
      0 => 'field_module_selection',
      1 => 'taxonomy_vocabulary_5',
      2 => 'field_projects',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Technical specifications',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => ' group-modules field-group-fieldset',
        'description' => 'Use the autocomplete widget to select significant contributed projects used, and expand on their use in the text area that follows. Readers will benefit from greater understanding of what was selected and why. This also rewards those who contribute to modules, themes and distributions.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_modules|node|casestudy|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_modules|node|casestudy|teaser';
  $field_group->group_name = 'group_modules';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'casestudy';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Modules/Themes/Distributions',
    'weight' => '3',
    'children' => array(
      0 => 'field_module_selection',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-modules field-group-hidden',
      ),
    ),
  );
  $field_groups['group_modules|node|casestudy|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Modules/Themes/Distributions');
  t('Technical specifications');
  t('Technical specifications inner');

  return $field_groups;
}
