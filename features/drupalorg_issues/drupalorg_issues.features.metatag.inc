<?php

/**
 * @file
 * drupalorg_issues.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function drupalorg_issues_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:project_issue.
  $config['node:project_issue'] = array(
    'instance' => 'node:project_issue',
    'config' => array(
      'image_src' => array(
        'value' => 'https://www.drupal.org/sites/all/themes/bluecheese/images/og.jpg',
      ),
    ),
  );

  return $config;
}
