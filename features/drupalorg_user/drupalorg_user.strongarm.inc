<?php

/**
 * @file
 * drupalorg_user.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drupalorg_user_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_user__user';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'googleanalytics' => array(
          'weight' => 10,
        ),
        'redirect' => array(
          'weight' => 39,
        ),
        'account' => array(
          'weight' => '0',
        ),
        'timezone' => array(
          'weight' => '4',
        ),
        'picture' => array(
          'weight' => '21',
        ),
        'contact' => array(
          'weight' => 4,
        ),
        'ckeditor' => array(
          'weight' => '10',
        ),
        'metatags' => array(
          'weight' => '40',
        ),
        'mollom' => array(
          'weight' => '99',
        ),
        'locale' => array(
          'weight' => 8,
        ),
      ),
      'display' => array(
        'summary' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_companies_worked_for' => array(
          'default' => array(
            'weight' => '11',
            'visible' => TRUE,
          ),
        ),
        'versioncontrol_project_user_commits' => array(
          'default' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
        ),
        'drupalorg_membership' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_user_issue_credit' => array(
          'default' => array(
            'weight' => '14',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_documentation_guides' => array(
          'default' => array(
            'weight' => '7',
            'visible' => TRUE,
          ),
        ),
        'bakery_primary_profile' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_user_contributor_roles' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_user_vetted' => array(
          'default' => array(
            'weight' => '8',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_users_maintained_projects' => array(
          'default' => array(
            'weight' => '15',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_user_git_link' => array(
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
        ),
        'drupalorg_user_events' => array(
          'default' => array(
            'weight' => '10',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_user__user'] = $strongarm;

  return $export;
}
