<?php

/**
 * @file
 * drupalorg_drupal8_panes.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function drupalorg_drupal8_panes_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'drupalorg_trydrupal_button';
  $content->admin_title = 'Try drupal button';
  $content->admin_description = '';
  $content->category = 'Drupal.org Project';
  $content->settings = array(
    'admin_title' => 'Try drupal button',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<span class="button"><a href="/try-drupal">Try a hosted Drupal demo</a></span>',
    'format' => '1',
    'substitute' => 1,
  );
  $export['drupalorg_trydrupal_button'] = $content;

  return $export;
}
