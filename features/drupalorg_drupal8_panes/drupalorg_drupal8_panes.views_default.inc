<?php

/**
 * @file
 * drupalorg_drupal8_panes.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function drupalorg_drupal8_panes_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'drupalcares';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Drupal Association supporting/partner organizations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Logo */
  $handler->display->display_options['fields']['field_logo']['id'] = 'field_logo';
  $handler->display->display_options['fields']['field_logo']['table'] = 'field_data_field_logo';
  $handler->display->display_options['fields']['field_logo']['field'] = 'field_logo';
  $handler->display->display_options['fields']['field_logo']['label'] = '';
  $handler->display->display_options['fields']['field_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_logo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_logo']['settings'] = array(
    'image_style' => 'grid-3-2x',
    'image_link' => 'content',
  );
  /* Sort criterion: Content: Org contribution rank (field_org_contribution_rank) */
  $handler->display->display_options['sorts']['field_org_contribution_rank_value']['id'] = 'field_org_contribution_rank_value';
  $handler->display->display_options['sorts']['field_org_contribution_rank_value']['table'] = 'field_data_field_org_contribution_rank';
  $handler->display->display_options['sorts']['field_org_contribution_rank_value']['field'] = 'field_org_contribution_rank_value';
  $handler->display->display_options['sorts']['field_org_contribution_rank_value']['order'] = 'DESC';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'organization' => 'organization',
  );
  /* Filter criterion: Content: Drupal Association supporting program (field_organization_support) */
  $handler->display->display_options['filters']['field_organization_support_value']['id'] = 'field_organization_support_value';
  $handler->display->display_options['filters']['field_organization_support_value']['table'] = 'field_data_field_organization_support';
  $handler->display->display_options['filters']['field_organization_support_value']['field'] = 'field_organization_support_value';
  $handler->display->display_options['filters']['field_organization_support_value']['value'] = array(
    'drupalcares_2020' => 'drupalcares_2020',
  );
  $handler->display->display_options['filters']['field_organization_support_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['operator_id'] = 'field_organization_support_value_op';
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['label'] = 'Drupal Association supporting program';
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['operator'] = 'field_organization_support_value_op';
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['identifier'] = 'field_organization_support_value';
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_organization_support_value']['expose']['remember_roles'] = array(
    2 => '2',
  );

  /* Display: Logos only */
  $handler = $view->new_display('panel_pane', 'Logos only', 'panel_pane_1');
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'field_organization_support_value' => array(
      'type' => 'user',
      'context' => 'entity:comment.field-attribute-contribution-to',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Drupal Association supporting program',
    ),
  );

  /* Display: Logos & names */
  $handler = $view->new_display('panel_pane', 'Logos & names', 'panel_pane_2');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Logo */
  $handler->display->display_options['fields']['field_logo']['id'] = 'field_logo';
  $handler->display->display_options['fields']['field_logo']['table'] = 'field_data_field_logo';
  $handler->display->display_options['fields']['field_logo']['field'] = 'field_logo';
  $handler->display->display_options['fields']['field_logo']['label'] = '';
  $handler->display->display_options['fields']['field_logo']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_logo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_logo']['settings'] = array(
    'image_style' => 'grid-3-2x',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['view_node']['alter']['text'] = '<div>[field_logo]</div>[title]';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'field_organization_support_value' => array(
      'type' => 'user',
      'context' => 'entity:comment.field-attribute-contribution-to',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Drupal Association supporting program',
    ),
  );
  $translatables['drupalcares'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Drupal Association supporting program'),
    t('Logos only'),
    t('View panes'),
    t('Logos & names'),
    t('<div>[field_logo]</div>[title]'),
  );
  $export['drupalcares'] = $view;

  return $export;
}
