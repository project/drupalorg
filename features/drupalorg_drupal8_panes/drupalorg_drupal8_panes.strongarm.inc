<?php

/**
 * @file
 * drupalorg_drupal8_panes.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drupalorg_drupal8_panes_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_node_page';
  $strongarm->value = array(
    'status' => 1,
    'help' => '',
    'view modes' => array(
      'page_manager' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
      'default' => array(
        'status' => 1,
        'substitute' => '',
        'default' => 1,
        'choice' => 1,
      ),
      'teaser' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
      'nodechanges' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
      'issuemetadata' => array(
        'status' => 0,
        'substitute' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_node_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page:default_selection';
  $strongarm->value = 'node:page:default:default';
  $export['panelizer_node:page:default_selection'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page_allowed_layouts';
  $strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:19:"panelizer_node:page";s:23:"allowed_layout_settings";a:12:{s:8:"flexible";b:0;s:17:"threecol_33_34_33";b:0;s:13:"twocol_bricks";b:0;s:6:"onecol";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:14:"twocol_stacked";b:0;s:17:"threecol_25_50_25";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:6:"twocol";b:0;s:5:"hydra";b:1;s:9:"capricorn";b:1;s:6:"cygnus";b:1;}s:10:"form_state";N;}';
  $export['panelizer_node:page_allowed_layouts'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page_allowed_layouts_default';
  $strongarm->value = 0;
  $export['panelizer_node:page_allowed_layouts_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page_allowed_types';
  $strongarm->value = array(
    'node_comment_wrapper-node_comment_wrapper' => TRUE,
    'node_links-node_links' => TRUE,
    'cookie_consent_reset-cookie_consent_reset' => TRUE,
    'steward_pricing-steward_pricing' => TRUE,
    'community_stats-community_stats' => TRUE,
    'donors-donors' => TRUE,
    'entity_form_field-comment:comment_body' => TRUE,
    'custom-custom' => TRUE,
    'custom-d8_landing_page_ad_1' => TRUE,
    'custom-d8_landing_page_ad_2' => TRUE,
    'custom-d8_landing_page_ad_3' => TRUE,
    'custom-d8_sponsor_header' => TRUE,
    'custom-roadmap_view_roadmap' => TRUE,
    'custom-roadmap_criteria_learn_more' => TRUE,
    'custom-roadmap_volunteer_learn_more' => TRUE,
    'custom-roadmap_community_learn_more' => TRUE,
    'custom-roadmap_sidebar_footer' => TRUE,
    'custom-drupalorg_section_notice' => TRUE,
    'custom-d8_top_sponsor' => TRUE,
    'custom-d8_sub_sponsor' => TRUE,
    'custom-lingotek_multilingual_ad' => TRUE,
    'custom-drupalcares_thanks_organizations' => TRUE,
    'custom-drupal_cares' => TRUE,
    'custom-discuss_help' => TRUE,
    'custom-drop_is_moving_socials' => TRUE,
    'custom-divider' => TRUE,
    'custom-drupal_8_documentation_links' => TRUE,
    'custom-drupalorg_contribute_documentation' => TRUE,
    'custom-drupalorg_trydrupal_button' => TRUE,
    'block-block-161' => TRUE,
    'block-fieldable_panels_panes-872' => TRUE,
    'block-mailchimp_signup-drupal_email_list' => TRUE,
    'block-mailchimp_signup-member_emails' => TRUE,
    'block-menu-menu-d7-eol-filter-menu' => TRUE,
    'block-menu-menu-og-2614904' => TRUE,
    'block-menu-menu-og-2743405' => TRUE,
    'block-menu-menu-og-2818027' => TRUE,
    'block-og_menu-og_single_menu_block' => TRUE,
    'block-drupalorg-carbon-ad-sidebar' => TRUE,
    'block-views-111a95b0421bc26dea38205bce7e0396' => TRUE,
    'block-views-ea33df8468481d8da6df22351817cb23' => TRUE,
    'block-views-dc6eca7eaa9e5c27a7734470527d3a0e' => TRUE,
    'block-views-2369ad5b42eaaed148e421af93f74aca' => TRUE,
    'fieldable_panels_pane-case_study' => TRUE,
    'fieldable_panels_pane-cta' => TRUE,
    'fieldable_panels_pane-cta_multiple' => TRUE,
    'fieldable_panels_pane-fpid:487' => TRUE,
    'fieldable_panels_pane-fpid:562' => TRUE,
    'fieldable_panels_pane-fpid:692' => TRUE,
    'fieldable_panels_pane-fpid:693' => TRUE,
    'fieldable_panels_pane-fpid:694' => TRUE,
    'fieldable_panels_pane-fpid:734' => TRUE,
    'fieldable_panels_pane-fpid:835' => TRUE,
    'fieldable_panels_pane-fpid:872' => TRUE,
    'fieldable_panels_pane-fpid:874' => TRUE,
    'fieldable_panels_pane-fpid:891' => TRUE,
    'fieldable_panels_pane-fpid:892' => TRUE,
    'fieldable_panels_pane-fpid:1145' => TRUE,
    'fieldable_panels_pane-fpid:1146' => TRUE,
    'fieldable_panels_pane-fpid:1148' => TRUE,
    'fieldable_panels_pane-fpid:1149' => TRUE,
    'fieldable_panels_pane-fpid:1150' => TRUE,
    'fieldable_panels_pane-fpid:446' => TRUE,
    'fieldable_panels_pane-fpid:732' => TRUE,
    'fieldable_panels_pane-fpid:880' => TRUE,
    'views-community_events' => TRUE,
    'views_panes-blog-panel_pane_1' => TRUE,
    'views_panes-community_events-comm_events_confirmed_pane' => TRUE,
    'views_panes-community_events-comm_events_proposed_pane' => TRUE,
    'views_panes-community_events-comm_events_cfp_pane' => TRUE,
    'views_panes-community_events-comm_events_completed_pane' => TRUE,
    'views_panes-community_events-drupalfest_events_confirmed' => TRUE,
    'views_panes-community_events-confirmed_event_map' => TRUE,
    'views_panes-community_events-pane_community_events_table' => TRUE,
    'views_panes-drupalcares-panel_pane_1' => TRUE,
    'views_panes-drupalcares-panel_pane_2' => TRUE,
    'views_panes-drupalorg_logos-panel_pane_1' => TRUE,
    'views_panes-drupalorg_logos-panel_pane_2' => TRUE,
    'views_panes-drupalorg_logos-panel_pane_3' => TRUE,
    'views_panes-drupalorg_logos-panel_pane_4' => TRUE,
    'views_panes-drupalorg_organizations-panel_pane_1' => TRUE,
    'views_panes-drupalorg_organizations-migration_partner_pane' => TRUE,
    'views_panes-drupalorg_organizations-d7_eol_pane_hd' => TRUE,
    'views_panes-getting_involved_reference_blocks-panel_role_browser' => TRUE,
    'views_panes-getting_involved_reference_blocks-panel_skill_browser' => TRUE,
    'views_panes-getting_involved_reference_blocks-panel_task_browser' => TRUE,
    'views_panes-getting_involved_reference_blocks-panel_in_area' => TRUE,
    'views_panes-getting_involved_reference_blocks-panel_skills_used_area' => TRUE,
    'views_panes-redesign_2018_case_studies-panel_pane_1' => TRUE,
  );
  $export['panelizer_node:page_allowed_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page_allowed_types_default';
  $strongarm->value = 0;
  $export['panelizer_node:page_allowed_types_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:page_default';
  $strongarm->value = array(
    'custom' => 'custom',
    'token' => 0,
    'entity_form_field' => 0,
    'entity_field' => 0,
    'entity_field_extra' => 0,
    'block' => 0,
    'entity_view' => 0,
    'fieldable_panels_pane' => 0,
    'flag_link' => 0,
    'views' => 0,
    'views_panes' => 0,
    'other' => 0,
  );
  $export['panelizer_node:page_default'] = $strongarm;

  return $export;
}
