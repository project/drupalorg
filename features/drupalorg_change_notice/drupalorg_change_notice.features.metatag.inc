<?php

/**
 * @file
 * drupalorg_change_notice.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function drupalorg_change_notice_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:changenotice.
  $config['node:changenotice'] = array(
    'instance' => 'node:changenotice',
    'config' => array(
      'description' => array(
        'value' => '[node:field_description]',
      ),
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
    ),
  );

  return $config;
}
