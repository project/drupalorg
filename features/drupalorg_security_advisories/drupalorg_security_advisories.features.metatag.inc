<?php

/**
 * @file
 * drupalorg_security_advisories.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function drupalorg_security_advisories_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:sa.
  $config['node:sa'] = array(
    'instance' => 'node:sa',
    'config' => array(
      'description' => array(
        'value' => '[node:field_sa_description]',
      ),
    ),
  );

  return $config;
}
