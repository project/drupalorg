<?php

/**
 * @file
 * drupalorg_projects.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function drupalorg_projects_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main|node|project_release|default';
  $field_group->group_name = 'group_main';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project_release';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main',
    'weight' => '24',
    'children' => array(
      0 => 'body',
      1 => 'field_release_files',
      2 => 'drupalorg_composer_install',
      3 => 'drupalorg_release_security_advisories',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Main',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-release-main',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_main|node|project_release|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_release_meta|node|project_release|teaser';
  $field_group->group_name = 'group_release_meta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project_release';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Release metadata',
    'weight' => '-1',
    'children' => array(
      0 => 'taxonomy_vocabulary_7',
      1 => 'field_release_vcs_label',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Release metadata',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-release-meta',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_release_meta|node|project_release|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar_top|node|project_release|default';
  $field_group->group_name = 'group_sidebar_top';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project_release';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Top sidebar',
    'weight' => '0',
    'children' => array(
      0 => 'release_try_drupal',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Top sidebar',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-sidebar-top',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_sidebar_top|node|project_release|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar|node|project_release|default';
  $field_group->group_name = 'group_sidebar';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project_release';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sidebar',
    'weight' => '27',
    'children' => array(
      0 => 'taxonomy_vocabulary_7',
      1 => 'field_release_vcs_label',
      2 => 'drupalorg_release_core_whats_next',
      3 => 'project_release_info',
      4 => 'project_release_other_releases',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Sidebar',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-release-sidebar',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_sidebar|node|project_release|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Main');
  t('Release metadata');
  t('Sidebar');
  t('Top sidebar');

  return $field_groups;
}
