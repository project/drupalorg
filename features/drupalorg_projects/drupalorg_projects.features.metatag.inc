<?php

/**
 * @file
 * drupalorg_projects.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function drupalorg_projects_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:project_distribution.
  $config['node:project_distribution'] = array(
    'instance' => 'node:project_distribution',
    'disabled' => FALSE,
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_project_images]',
      ),
    ),
  );

  // Exported Metatag config instance: node:project_drupalorg.
  $config['node:project_drupalorg'] = array(
    'instance' => 'node:project_drupalorg',
    'disabled' => FALSE,
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_project_images]',
      ),
    ),
  );

  // Exported Metatag config instance: node:project_general.
  $config['node:project_general'] = array(
    'instance' => 'node:project_general',
    'disabled' => FALSE,
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_project_images]',
      ),
    ),
  );

  // Exported Metatag config instance: node:project_module.
  $config['node:project_module'] = array(
    'instance' => 'node:project_module',
    'disabled' => FALSE,
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_project_images]',
      ),
    ),
  );

  // Exported Metatag config instance: node:project_theme.
  $config['node:project_theme'] = array(
    'instance' => 'node:project_theme',
    'disabled' => FALSE,
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_project_images]',
      ),
    ),
  );

  return $config;
}
