<?php

/**
 * @file
 * drupalorg_documentation.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function drupalorg_documentation_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'discuss_help';
  $content->admin_title = 'Documentation discuss help';
  $content->admin_description = 'Shown at the top of documentation discuss pages.';
  $content->category = 'Drupal.org';
  $content->settings = array(
    'admin_title' => '',
    'title' => '',
    'title_heading' => 'h2',
    'body' => 'This page is a change log for %node:title documentation page, and a space for discussing suggested improvements to the documentation.

<strong>If you are looking for help with your Drupal site, please visit the <a href="/support">Support page</a></strong> to learn about online chat, forums, and other support resources. Off-topic and spam comments will be deleted.',
    'format' => '1',
    'substitute' => 1,
  );
  $export['discuss_help'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'drupal_8_documentation_links';
  $content->admin_title = 'Drupal docs links';
  $content->admin_description = '';
  $content->category = 'Drupal.org';
  $content->settings = array(
    'admin_title' => 'Drupal 8 documentation links',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<h4>Curated Guides</h4>
<ul>
<li><a href="/docs/user_guide/en/index.html">Drupal User Guide</a></li>
<li><a href="/docs/official_docs/en/_evaluator_guide.html">Evaluator Guide</a></li>
<li><a href="/docs/official_docs/en/_local_development_guide.html">Local Development Guide</a></li>
</ul>

<h4>Drupal Wiki</h4>
<ul>
<li><a href="/docs">Drupal</a></li>
<li><a href="/docs/develop">Develop</a></li>
<li><a href="/docs/7">Drupal 7</a></li>
</ul>

<h4>API Info</h4>
<ul>
<li><a href="https://api.drupal.org/api/drupal">Complete API Reference</a></li>
<li><a href="/docs/drupal-apis">Drupal APIs</a></li>
</ul>

<h4><a href="/books">Drupal books</a></h4>',
    'format' => '1',
    'substitute' => 0,
  );
  $export['drupal_8_documentation_links'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'drupalorg_contribute_documentation';
  $content->admin_title = 'Contribute to documentation';
  $content->admin_description = '';
  $content->category = 'Drupal.org';
  $content->settings = array(
    'admin_title' => 'Contribute to documentation',
    'title' => 'Contribute',
    'title_heading' => 'h2',
    'body' => '<p>Read the <a href="/contribute/documentation" rel="nofollow">contribution guidelines</a> and consider joining the <a href="/about/strategic-initiatives/documentation" rel="nofollow">documentation initiative</a>.</p>',
    'format' => '1',
    'substitute' => 1,
  );
  $export['drupalorg_contribute_documentation'] = $content;

  return $export;
}
