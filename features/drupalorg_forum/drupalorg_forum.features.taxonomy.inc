<?php

/**
 * @file
 * drupalorg_forum.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drupalorg_forum_taxonomy_default_vocabularies() {
  return array(
    'vocabulary_1' => array(
      'name' => 'Forums',
      'machine_name' => 'vocabulary_1',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'forum',
      'weight' => -10,
    ),
  );
}
