<?php

/**
 * @file
 * drupalorg_content_types.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function drupalorg_content_types_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'divider';
  $content->admin_title = 'Divider';
  $content->admin_description = '';
  $content->category = 'Drupal.org';
  $content->settings = array(
    'admin_title' => 'Divider',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<hr><br>',
    'format' => '1',
    'substitute' => 0,
  );
  $export['divider'] = $content;

  return $export;
}
