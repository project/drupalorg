<?php

/**
 * @file
 * drupalorg_content_types.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function drupalorg_content_types_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:event.
  $config['node:event'] = array(
    'instance' => 'node:event',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_event_logo]',
      ),
    ),
  );

  // Exported Metatag config instance: node:page.
  $config['node:page'] = array(
    'instance' => 'node:page',
    'config' => array(
      'og:image' => array(
        'value' => 'https://www.drupal.org/sites/all/themes/bluecheese/images/og.jpg',
      ),
    ),
  );

  // Exported Metatag config instance: node:post.
  $config['node:post'] = array(
    'instance' => 'node:post',
    'config' => array(
      'image_src' => array(
        'value' => 'https://www.drupal.org/sites/all/themes/bluecheese/images/og.jpg',
      ),
    ),
  );

  // Exported Metatag config instance: node:section.
  $config['node:section'] = array(
    'instance' => 'node:section',
    'config' => array(
      'image_src' => array(
        'value' => 'https://www.drupal.org/sites/all/themes/bluecheese/images/og.jpg',
      ),
    ),
  );

  return $config;
}
