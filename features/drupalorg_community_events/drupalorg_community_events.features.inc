<?php

/**
 * @file
 * drupalorg_community_events.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalorg_community_events_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
