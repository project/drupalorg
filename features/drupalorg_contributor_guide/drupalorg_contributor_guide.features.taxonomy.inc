<?php

/**
 * @file
 * drupalorg_contributor_guide.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drupalorg_contributor_guide_taxonomy_default_vocabularies() {
  return array(
    'contribution_area' => array(
      'name' => 'Contribution area',
      'machine_name' => 'contribution_area',
      'description' => 'An area for contributors to contribute to, such as Documentation, Drupal Core, or Community',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
