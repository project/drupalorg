<?php

/**
 * @file
 * drupalorg_contributor_guide.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function drupalorg_contributor_guide_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_time_commitment|node|contributor_role|form';
  $field_group->group_name = 'group_time_commitment';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contributor_role';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Time commitment',
    'weight' => '12',
    'children' => array(
      0 => 'field_duration',
      1 => 'field_average_time',
      2 => 'field_commitment_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-time-commitment field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_time_commitment|node|contributor_role|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_time|node|contributor_role|default';
  $field_group->group_name = 'group_time';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'contributor_role';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Time commitment',
    'weight' => '8',
    'children' => array(
      0 => 'field_duration',
      1 => 'field_average_time',
      2 => 'field_commitment_description',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'Time commitment',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-time field-group-fieldset field-label-above',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_time|node|contributor_role|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Time commitment');

  return $field_groups;
}
