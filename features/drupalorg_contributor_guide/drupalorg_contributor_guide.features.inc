<?php

/**
 * @file
 * drupalorg_contributor_guide.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupalorg_contributor_guide_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drupalorg_contributor_guide_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function drupalorg_contributor_guide_node_info() {
  $items = array(
    'contributor_role' => array(
      'name' => t('Contributor role'),
      'base' => 'node_content',
      'description' => t('A contributor role that is documented as part of the Getting Involved Guide. A Role is a "position" in the community (short- or long-term).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A Role is a "position" in the community (short- or long-term). The title of a Role should be a noun, like "Documentation editor" or "Drupal core subsystem maintainer"'),
    ),
    'contributor_skill' => array(
      'name' => t('Contributor skill'),
      'base' => 'node_content',
      'description' => t('A contributor skill that is documented as part of the Getting Involved Guide. A Skill gives you the knowledge to perform a Task or fill a Role.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A Skill gives you the knowledge to perform a Task or fill a Role, whereas a Task is a set of defined steps with a defined goal. Links to tasks and roles that use or improve the skill will be shown automatically, so they do not need to be mentioned when editing/creating a Skill page. The title of a Skill should be a noun or -ing verb, like "PHP Programming" or "Project management" or "Public speaking"'),
    ),
    'contributor_task' => array(
      'name' => t('Contributor task'),
      'base' => 'node_content',
      'description' => t('A contributor task that is documented as part of the Getting Involved Guide. A Task has a well-defined goal and well-defined steps.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A Task has a well-defined goal and well-defined steps; if you follow the steps, you will achieve the goal. The title of a Task should start with a verb, like "Edit a documentation page" or "Test a patch".'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
