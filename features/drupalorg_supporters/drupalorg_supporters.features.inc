<?php

/**
 * @file
 * drupalorg_supporters.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupalorg_supporters_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
