<?php

/**
 * @file
 * drupalorg_supporters.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function drupalorg_supporters_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_organization_support'.
  $field_bases['field_organization_support'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_organization_support',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'supporting_enterprise' => 'Enterprise Organization Member',
        'supporting_signature' => 'Signature Organization Member',
        'supporting_premium' => 'Premium Organization Member',
        'supporting' => 'Classic Organization Member',
        'community_supporter' => 'Community Organization Member',
        'supporting_customer_charter' => 'Charter Customer Supporting Partner',
        'supporting_customer' => 'Customer Supporting Partner',
        'certified_top_tier' => 'Top Tier Drupal Certified Partner',
        'certified_diamond' => 'Diamond Drupal Certified Partner',
        'certified_platinum' => 'Platinum Drupal Certified Partner',
        'certified_gold' => 'Gold Drupal Certified Partner',
        'certified_silver' => 'Silver Drupal Certified Partner',
        'certified_bronze' => 'Bronze Drupal Certified Partner',
        'drupalcares_2020_premiere' => 'DrupalCares Premiere Supporter',
        'drupalcares_2020_champion' => 'DrupalCares Champion Supporter',
        'drupalcares_2020' => 'DrupalCares Supporter',
        'drupalcon_na_2023_presenting' => 'DrupalCon Pittsburgh 2023 Presenting Sponsor',
        'drupalcon_na_2023_champion' => 'DrupalCon Pittsburgh 2023 Champion Sponsor',
        'drupalcon_na_2023_advocate' => 'DrupalCon Pittsburgh 2023 Advocate Sponsor',
        'drupalcon_na_2023_exhibitor' => 'DrupalCon Pittsburgh 2023 Exhibitor Sponsor',
        'drupalcon_na_2023_module' => 'DrupalCon Pittsburgh 2023 Module Sponsor',
        'discover-drupal-sponsor' => 'Discover Drupal: Sponsor',
        'migration_partner' => 'Migration Certified Partner',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_short_description'.
  $field_bases['field_short_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_short_description',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 45,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
