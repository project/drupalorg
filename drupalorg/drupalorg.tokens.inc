<?php

/**
 * Implements hook_token_info();
 */
function drupalorg_token_info() {
  return [
    'tokens' => [
      'node' => [
        'drupalorg-documentation-context' => [
          'name' => t('Context for a documentation page or guide'),
          'description' => t('The parent of a documentation page or guide, and the top-level guide.'),
          'type' => 'node',
        ],
        'drupalorg-top-guide' => [
          'name' => t('Top-level guide'),
          'description' => t('Top-level parent of a documentation guide or page.'),
          'type' => 'node',
        ],
        'drupalorg-sa-id' => [
          'name' => t('Security advisory ID'),
          'description' => t('Such as “SA-CONTRIB-2017-077” or “SA-CORE-2014-005”'),
          'type' => 'node',
        ],
      ],
      'user' => [
        'drupalorg-current-organization-name' => [
          'name' => t('Current organization name'),
          'description' => t('Name of the <em>first</em> current organization a user has.')
        ],
        'drupalorg-first-or-user-name' => [
          'name' => t('First or user name'),
          'description' => t('If the user has a first name, that name, otherwise their username.'),
        ],
        'drupalorg-country-or-geolocation' => [
          'name' => t('Country with geolocation fallback'),
          'description' => t('The user’s country, or geolocation as a fallback.'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function drupalorg_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];

  switch ($type) {
    case 'node':
      if (isset($data['node'])) {
        $node_wrapper = entity_metadata_wrapper('node', $data['node']);

        if (($replace_tokens = token_find_with_prefix($tokens, 'drupalorg-top-guide')) || isset($tokens['drupalorg-documentation-context'])) {
          try {
            if ($parents = drupalorg_get_parents($data['node'])) {
              $top_guide = array_pop($parents);
            }
            else {
              $top_guide = $data['node'];
            }

            // Top level guide (node).
            if (!empty($replace_tokens)) {
              $replacements += token_generate('node', $replace_tokens, ['node' => $top_guide]);
            }

            // Documentation context (text).
            if (isset($tokens['drupalorg-documentation-context'])) {
              if (empty($parents)) {
                $replacement = '';
              }
              else {
                $parent = array_shift($parents);
                $replacement = check_plain($parent->title) . ' | ';
              }
              $replacements[$tokens['drupalorg-documentation-context']] = $replacement . t('@title guide', [
                '@title' => $top_guide->title,
              ]);
            }
          }
          catch (Exception $e) {}
        }

        if (isset($tokens['drupalorg-sa-id']) && $data['node']->type === 'sa') {
          $replacements[$tokens['drupalorg-sa-id']] = drupalorg_sa_id($node_wrapper);
        }
      }
      break;

    case 'user':
      if (isset($data['user'])) {
        $wrapper = entity_metadata_wrapper('user', $data['user']);

        // Current organization name.
        if (isset($tokens['drupalorg-current-organization-name'])) {
          foreach ($wrapper->field_organizations as $organization) {
            try {
              if ($organization->field_current->value()) {
                $replacements[$tokens['drupalorg-current-organization-name']] = $organization->field_organization_name->value();
              }
            }
            catch (EntityMetadataWrapperException $e) { }
          }
        }

        // First or user name.
        if (isset($tokens['drupalorg-first-or-user-name'])) {
          $replacements[$tokens['drupalorg-first-or-user-name']] = empty($wrapper->field_first_name->value()) ? $data['user']->name : $wrapper->field_first_name->value();
        }

        // Country or geolocation.
        if (isset($tokens['drupalorg-country-or-geolocation'])) {
          if (empty($wrapper->field_country->value())) {
            if ($country = drupalorg_crosssite_get_and_vary_header('GeoIP-country')) {
              $country_names = list_allowed_values(field_info_field('field_country'));
              if (isset($country_names[$country])) {
                $replacements[$tokens['drupalorg-country-or-geolocation']] = $country_names[$country];
              }
            }
          }
          else {
            $replacements[$tokens['drupalorg-country-or-geolocation']] = $wrapper->field_country->label();
          }
        }
      }
      break;
  }

  return $replacements;
}
