<?php

use Gitlab\Exception\RuntimeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;


/**
 * Class TugboatPreview
 *
 * Provides an interface to construct and build Tugboat previews.
 */
class TugboatPreview {

  /**
   * The source branch name.
   *
   * @var string
   */
  private $source_branch;

  /**
   * The target branch name.
   *
   * @var string
   */
  private $target_branch;

  /**
   * The preview configuration.
   *
   * @var array
   */
  private $configuration;

  /**
   * The merge request identifier.
   *
   * @var string
   */
  private $merge_request_id;

  /**
   * The Tugboat repository id.
   *
   * @var string
   */
  private $repository_id;

  /**
   * The repository namespace.
   *
   * @var string
   */
  private $repository_namespace;

  /**
   * The repository name.
   *
   * @var string
   */
  private $repository_name;

  /**
   * TugboatPreview constructor.
   *
   * @param array $configuration
   *   The Tugboat preview configuration.
   * @param string $merge_request_id
   *   The merge request id.
   * @param string $repository_namespace
   *   The repository namespace.
   * @param string $repository_name
   *   The repository name.
   * @param string $repository_id
   *   The repository identifier.
   * @param string $source_branch
   *   The merge request's source branch.
   * @param $target_branch
   *   The merge request's target branch.
   */
  public function __construct($configuration, $merge_request_id, $repository_namespace, $repository_name, $repository_id, $source_branch, $target_branch) {
    $this->configuration = $configuration;
    $this->merge_request_id = $merge_request_id;
    $this->repository_namespace = $repository_namespace;
    $this->repository_name = $repository_name;
    $this->repository_id = $repository_id;
    $this->source_branch = $source_branch;
    $this->target_branch = $target_branch;
  }

  /**
   * Builds a Tugboat preview out of a GitLab payload.
   *
   * Tugboat config files for core are at https://github.com/TugboatQA/drupalorg/tree/master/drupal.
   *
   * Contrib modules can provide a .tugboat/config.yml file.
   *
   * @param object $payload
   *   A GitLab payload.
   *
   * @return \TugboatPreview
   *   A TugboatPreview object.
   *
   * @throws \UnsupportedMergeRequestEventException
   *   If the merge request event is unsupported.
   * @throws \ProjectDoesNotSupportTugboatException
   *   If the project does not support Tugboat.
   * @throws \Exception
   *   If there is an error.
   */
  public static function fromGitLabPayload($payload) {
    // Only build on new or reopened merge requests.
    if (!in_array($payload->object_attributes->action, ['open', 'reopen'])) {
      throw new UnsupportedMergeRequestEventException();
    }

    $repository_namespace = $payload->project->namespace;
    $repository_name = $payload->project->name;
    $merge_request_id = $payload->object_attributes->iid;
    $source_branch = $payload->object_attributes->source_branch;
    $target_branch = $payload->object_attributes->target_branch;

    if (self::projectSupportsTugboat($payload)) {
      $configuration = [];
    }
    elseif ($repository_name == 'drupal') {
      $configuration = self::fetchConfiguration($repository_name, $target_branch);
    }

    if (!isset($configuration)) {
      throw new ProjectDoesNotSupportTugboatException();
    }

    // Check and register repository.
    if (!($repository_id = TugboatApi::getRepositoryId($repository_namespace . '/' . $repository_name))) {
      try {
        self::addTugboatTo($payload->object_attributes->target_project_id);
        $repository_id = json_decode(TugboatApi::registerRepository($repository_namespace, $repository_name))->id;
      }
      catch (Exception $e) {
        throw $e;
      }
      finally {
        self::removeTugboatFrom($payload->object_attributes->target_project_id);
      }
    }

    return new self($configuration, $merge_request_id, $repository_namespace, $repository_name, $repository_id, $source_branch, $target_branch);
  }

  /**
   * Adds the Tugboat GitLab user to a repository.
   *
   * @param int $project_id
   *   The GitLab project identifier.
   *
   * @throws \Exception
   *   If there was an error adding the member.
   */
  private static function addTugboatTo($project_id) {
    $maintainer_access_level = 40;
    $tugboat_gitlab_user_id = variable_get('tugboat_gitlab_user_id');
    try {
      $user = versioncontrol_gitlab_get_client()->api('projects')->addMember($project_id, $tugboat_gitlab_user_id, $maintainer_access_level);
    }
    catch (RuntimeException $e) {
    }

    if (empty($user)) {
      throw new Exception('Could not make Tugboat a member of the repository with id ' . $project_id);
    }
  }

  /**
   * Removes the Tugboat GitLab user from a repository.
   *
   * @param int $project_id
   *   The GitLab project identifier.
   */
  private static function removeTugboatFrom($project_id) {
    $tugboat_gitlab_user_id = variable_get('tugboat_gitlab_user_id');
    versioncontrol_gitlab_get_client()->api('projects')->removeMember($project_id, $tugboat_gitlab_user_id);
  }


  /**
   * Sends a request to the Tugboat API to build a preview.
   *
   * @return string
   *   The Response from the Tugboat API.
   *
   * @throws \Exception
   *   If there is an error.
   */
  public function build() {
    return TugboatApi::getClient()->post('/v3/previews', [
      'json' => $this->toPayload(),
    ]);
  }

  /**
   * Transforms the preview into a body payload.
   *
   * @return array
   *   The payload to build a preview.
   *
   * @throws \Exception
   *   If there was an issue with the Tugboat API.
   */
  public function toPayload() {
    $payload = [
      'ref' => $this->merge_request_id,
      'repo' => $this->repository_id,
      'name' => $this->source_branch,
      'expires' => date('c', strtotime('+5 days')),
      'type' => 'pullrequest',
    ];

    // Contrib projects have a Tugboat config file so we don't need to send it in the request.
    if (!empty($this->configuration)) {
      $payload['config'] = $this->configuration;
    }

    return $payload;
  }

  /**
   * Returns Tugboat configuration for a project and branch.
   *
   * Fetches configuration files from https://github.com/TugboatQA/drupalorg.
   *
   * @param string $project_name
   *   The project name, like drupal for Drupal core.
   * @param string $branch
   *   The branch name.
   *
   * @return array
   *   The Tugboat configuration.
   *
   * @throws \Exception
   *   When no configuration could be found or parsing failed.
   */
  public static function fetchConfiguration($project_name, $branch) {
    $tugboat_configuration = [];
    $client = new Client();

    $tugboat_config_repository = variable_get('tugboat_config_repository', 'TugboatQA/drupalorg');
    $tugboat_config_branch = variable_get('tugboat_config_branch', 'HEAD');
    $tugboat_configs_path = 'https://raw.githubusercontent.com/' . $tugboat_config_repository . '/' . $tugboat_config_branch . '/' . $project_name;
    $tugboat_config_file_url = $tugboat_configs_path . '/' . $branch . '/config.yml';
    try {
      $response_contents = $client->get($tugboat_config_file_url)->getBody()->__toString();
    }
    catch (ClientException $e) {}
    if (empty($response_contents)) {
      $tugboat_config_file_url = $tugboat_configs_path . '/default/config.yml';
      try {
        $response_contents = $client->get($tugboat_config_file_url)->getBody()->__toString();
      }
      catch (ClientException $e) {}
      if (empty($response_contents)) {
        throw new Exception('Failed to fetch default Tugboat configuration at ' . $tugboat_config_file_url);
      }
    }

    if (!empty($response_contents)) {
      try {
        $tugboat_configuration = Yaml::parse($response_contents);
      }
      catch (ParseException $e) {
        throw new Exception('Failed to parse the Tugboat configuration. Error is ' . $e->getMessage() . ' and configuration is ' . print_r($response_contents, TRUE));
      }
    }

    return $tugboat_configuration;
  }

  /**
   * Checks if a contrib project supports Tugboat.
   *
   * @param object $payload
   *   The request payload.
   *
   * @return bool
   *   TRUE if the project supports Tugboat. FALSE otherwise.
   */
  private static function projectSupportsTugboat($payload) {
    $project_tugboat_config_url = drupalorg_gitlab_url_with_htauth() . '/' . $payload->object_attributes->source->path_with_namespace . '/-/raw/' . $payload->object_attributes->source_branch . '/.tugboat/config.yml';
    $client = new Client();
    try {
      $configuration = $client->get($project_tugboat_config_url)->getBody()->__toString();
      return !empty($configuration);
    }
    catch (ClientException $e) {
      return FALSE;
    }
  }

}
