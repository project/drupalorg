<?php

class DrupalorgIssueFork extends Entity {

  /**
   * @var int Issue node ID of the issue this fork is for.
   */
  public $nid;

  /**
   * @var int Project node ID of the project being forked.
   */
  public $project_nid;

  /**
   * @var int GitLab project ID of the project created for this fork.
   */
  public $gitlab_project_id;

  /**
   * @var string Name of the project created for the fork.
   */
  public $name;

  /**
   * @var array Branches of the project, used by DrupalorgIssueFork::getBranches().
   */
  private $branches;

  /**
   * @var string GitLab URL for the fork’s parent project.
   */
  private $project_gitlab_url;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values = []) {
    parent::__construct($values, 'drupalorg_issue_fork');
  }

  /**
   * Create a new issue fork.
   *
   * @param EntityDrupalWrapper $node_wrapper
   *   An entity wrapper for an issue node.
   * @param string $branch
   *   If provided, create a new branch after forking.
   *
   * @return DrupalorgIssueFork
   *   Thew new issue fork.
   */
  public static function fork(EntityDrupalWrapper $node_wrapper, $branch = NULL) {
    $name = $node_wrapper->field_project->field_project_machine_name->value(['sanitize' => TRUE]) . '-' . $node_wrapper->getIdentifier();

    watchdog('drupalorg_issue_fork', 'Creating @name issue fork.', [
      '@name' => $name,
    ], WATCHDOG_NOTICE);

    // Create the fork in GitLab.
    $gitlab_api = versioncontrol_gitlab_get_client();
    $projects_api = $gitlab_api->api('projects');
    try {
      $fork_project = $projects_api->fork(versioncontrol_project_repository_load($node_wrapper->field_project->raw())->gitlab_project_id, [
        'namespace' => variable_get('versioncontrol_gitlab_namespace_ids', [])['issue'],
        'path' => $name,
        'name' => $name,
      ]);
    }
    catch (\Gitlab\Exception\RuntimeException $e) {
      if ($e->getCode() == 409 && $e->getMessage() === '"base" Failed to create wiki') {
        watchdog('drupalorg_issue_fork', 'Moving failed fork @name out of the way', ['@name' => $name]);
        $projects_api->remove('issue/' . $name);
        $projects_api->update('issue/' . $name, [
          'path' => $name . '-forking-failed-' . REQUEST_TIME,
          'name' => $name . '-forking-failed-' . REQUEST_TIME,
        ]);
      }
      throw $e;
    }

    // Save our record of the fork.
    $issue_fork = new DrupalorgIssueFork([
      'nid' => $node_wrapper->getIdentifier(),
      'project_nid' => $node_wrapper->field_project->getIdentifier(),
      'gitlab_project_id' => $fork_project['id'],
      'name' => $fork_project['name'],
    ]);
    $issue_fork->is_new = TRUE;
    $issue_fork->save();

    // Queue setting the GitLab project description, since that does not stick
    // until the initial forking has completed.
    $queue = DrupalQueue::get('drupalorg_issue_forks');
    if ($queue instanceof BeanstalkdQueue) {
      $queue->beanstalkd_params['delay'] = 1;
    }
    $queue->createItem([
      'fork' => $issue_fork,
      'task' => 'updateProject',
      'arguments' => [TRUE, $branch, $GLOBALS['user']->git_username],
    ]);

    // Update the issue service to point back to Drupal.org.
    $gitlab_api->getHttpClient()->put('projects/' . $issue_fork->gitlab_project_id . '/integrations/custom-issue-tracker', ['Content-Type' => 'application/json'], json_encode([
      'project_url' => url('node/' . $issue_fork->nid, ['absolute' => TRUE]),
      'issues_url' => url('i', ['absolute' => TRUE]) . '/:id',
      'new_issue_url' => url('node/add/project-issue/' . $issue_fork->getIssueWrapper()->field_project->field_project_machine_name->value(), ['absolute' => TRUE]),
      'use_inherited_settings' => 0,
    ]))->getBody();

    return $issue_fork;
  }

  /**
   * Update the project description in GitLab, if fork importing has finished.
   *
   * @param bool $initial
   *   TRUE for the initial update after forking.
   * @param string $branch
   *   If provided, create a new branch after forking.
   * @param string $git_username
   *   Required with $branch, the Git username of the person creating the
   *   branch.
   *
   * @return bool
   *   If the description was set, TRUE. If the project is still importing, FALSE.
   */
  public function updateProject($initial = FALSE, $branch = NULL, $git_username = NULL) {
    $project_info = $this->fetchProjectInfo();

    // Only update the project if “importing” has finished.
    if ($project_info['import_status'] === 'finished') {
      if ($branch !== NULL) {
        try {
          versioncontrol_gitlab_get_client($git_username)->api('repositories')->createBranch($this->gitlab_project_id, $branch, $this->getDevRelease()->field_release_vcs_label[LANGUAGE_NONE][0]['value']);
        }
        catch (Exception $e) {
          $uri = $this->uri();
          watchdog('drupalorg_issue_fork', 'Exception in initial branch creation for !issue: %msg', [
            '!issue' => l($this->label(), $uri['path'], $uri['options']),
            '%msg' => $e->getMessage(),
          ], WATCHDOG_ERROR);
          db_delete('drupalorg_issue_fork_branches')
            ->condition('gitlab_project_id', $this->gitlab_project_id)
            ->condition('branch', $branch)
            ->execute();
        }
      }

      $gitlab_api = versioncontrol_gitlab_get_client();
      $project_update = [
        'squash_option' => 'default_on',
        'issues_access_level' => 'disabled',
        'merge_requests_access_level' => 'enabled',
        'forking_access_level' => 'disabled',
        'builds_access_level' => 'enabled',
        'wiki_access_level' => 'disabled',
        'snippets_access_level' => 'disabled',
        'pages_access_level' => 'disabled',
        'operations_access_level' => 'disabled',
        'monitor_access_level' => 'disabled',
        'environments_access_level' => 'disabled',
        'feature_flags_access_level' => 'disabled',
        'infrastructure_access_level' => 'disabled',
        'releases_access_level' => 'disabled',
        'container_registry_enabled' => FALSE,
        'shared_runners_enabled' => TRUE,
        'request_access_enabled' => FALSE,
        'auto_devops_enabled' => FALSE,
        'packages_enabled' => FALSE,
        'service_desk_enabled' => FALSE,
        'remove_source_branch_after_merge' => FALSE,
      ];
      if ($initial) {
        // Issues can move between projects. Since the project name is in the
        // issue URL, leave it as-is after the initial import. Used as a signal
        // for issue fork completion in issue-fork.js. If “For collaboration”
        // is changed, also update issue-fork.js.
        $project_update['description'] = t('For collaboration on !url', [
          '!url' => url('node/' . $this->getIssueWrapper()->getIdentifier(), ['absolute' => TRUE]),
        ]);
      }
      $gitlab_api->api('projects')->update($this->gitlab_project_id, $project_update);

      // Workaround https://gitlab.com/gitlab-org/gitlab/-/issues/408746.
      $gitlab_api->getHttpClient()->post('projects/' . $this->gitlab_project_id . '/housekeeping', [
        'query' => ['task' => 'eager'],
      ]);

      return TRUE;
    }

    // If there is an import error, it probably will not finish.
    if (!empty($project_info['import_error'])) {
      throw new Exception(t('Import failed for @name: @import_error', [
        '@name' => $this->name,
        '@import_error' => $project_info['import_error'],
      ]));
    }

    return FALSE;
  }

  /**
   * Grant access to a user.
   *
   * @param object $account
   *   A Drupal user account object, which has GitLab access set up.
   */
  public function addMember(stdClass $account) {
    // 30 is “Developer access” https://docs.gitlab.com/ee/api/members.html
    versioncontrol_gitlab_get_client()->api('projects')->addMember($this->gitlab_project_id, versioncontrol_gitlab_get_user_id($account), '30');
    db_merge('drupalorg_issue_fork_maintainers')
      ->key([
        'gitlab_project_id' => $this->gitlab_project_id,
        'uid' => $account->uid,
      ])
      ->fields(['current_member' => 1])
      ->execute();
  }

  /**
   * React to GitLab merge request web hook events.
   *
   * @param object $body
   *   The web hook payload, see
   *   https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events
   *
   * @return bool
   *   If processing was successful.
   */
  public function receiveMergeRequestEvent(stdClass $body) {
    switch ($body->object_attributes->action) {
      case 'open':
        $comment = '!name opened !link';
        break;

      case 'reopen':
        $comment = '!name reopened !link';
        break;

      case 'close':
        $comment = '!name closed !link';
        break;
    }

    if (isset($comment)) {
      $node = node_load($this->nid, NULL, TRUE);
      $result = (new EntityFieldQuery())->entityCondition('entity_type', 'user')
        ->propertyCondition('git_username', $body->user->username)
        ->range(0, 1)->execute();
      if (!isset($result['user'])) {
        throw new Exception(format_string('Could not find Drupal user @username: <pre>@body</pre>', [
          '@username' => $body->user->username,
          '@body' => print_r($body, TRUE),
        ]));
      }
      $account = user_load(array_keys($result['user'])[0]);
      $followup_account = user_load(variable_get('project_issue_followup_user', 0));
      $node->nodechanges_comment['comment_body'][LANGUAGE_NONE][0] = [
        'value' => format_string($comment, [
          '!name' => l(format_username($account), 'user/' . $account->uid),
          '!link' => l(t('merge request !@iid', [
            '@iid' => $body->object_attributes->iid,
          ]), $body->object_attributes->url, ['external' => TRUE]),
        ]),
        'format' => filter_default_format($followup_account),
        'drupalorg_crosssite_ham' => TRUE,
      ];
      $node->nodechanges_uid = $followup_account->uid;
      node_save($node);

      if ($body->object_attributes->action === 'open') {
        // Save the initial cid.
        db_update('drupalorg_issue_fork_branches')
          ->condition('gitlab_project_id', $body->object_attributes->source_project_id)
          ->condition('branch', $body->object_attributes->source_branch)
          ->condition('merge_request_iid', $body->object_attributes->iid)
          ->fields([
            'merge_request_initial_cid' => $node->nodechanges_cid,
            'merge_request_target_branch' => $body->object_attributes->target_branch,
          ])
          ->execute();
      }
    }

    $this->buildTugboatPreview($body);

    return TRUE;
  }

  /**
   * Get information about an issue fork for display on the issue page.
   *
   * @return array
   *   A Drupal render array.
   */
  public function forkInformation() {
    $versioncontrol_gitlab_url = variable_get('versioncontrol_gitlab_url');
    drupal_add_html_head_link([
      'rel' => 'preconnect',
      'href' => $versioncontrol_gitlab_url,
      'crossorigin' => 'use-credentials',
      'as' => 'fetch',
    ], TRUE);
    $uri = $this->uri();
    $uri['options']['attributes']['class'][] = 'fork-link';
    $uri['options']['attributes']['title'] = t('View repository in GitLab');
    $output = [
      '#type' => 'container',
      '#attributes' => ['class' => ['drupalorg-issue-fork']],
      '#attached' => [
        'js' => array_merge([
          [
            'type' => 'setting',
            'data' => [
              'drupalorgGitlabUrl' => $versioncontrol_gitlab_url,
              'drupalorgTZ' => date_default_timezone_get(),
              'drupalorgIssueForks' => [$this->name => $this->gitlab_project_id],
            ],
          ],
          drupal_get_path('module', 'drupalorg') . '/js/issue-fork.js' => [
            'type' => 'file',
            'requires_jquery' => FALSE,
          ],
        ], drupalorg_copy_js()),
      ],
      'header' => [
        '#prefix' => '<div class="header-and-access"><h3>' . t('Issue fork !link', [
          '!link' => l($this->label(), $uri['path'], $uri['options']),
        ]) . '</h3> ',
        'access' => [],
        '#suffix' => '</div>',
      ],
      'help' => [
        '#prefix' => '<details class="help-commands"><summary><img src="' . url(drupal_get_path('module', 'drupalorg') . '/images/command-dk-gray.svg') . '" alt="' . t('Command icon') . '" class="icon-inline"> ' . t('Show commands') . '</summary>',
        'remote_add_ssh' => [
          '#prefix' => '<p>' . t('Start within a Git clone of the project using the <a href="!instructions">version control instructions</a>.', [
            '!instructions' => url('node/' . $this->project_nid . '/git-instructions'),
          ]) . '</p>',
          '#type' => 'textarea',
          '#title' => t('Add & fetch this issue fork’s repository'),
          '#rows' => 2,
          '#resizable' => FALSE,
          '#attributes' => [
            'class' => ['remote-add-ssh', 'drupalorg-copy'],
            'readonly' => 'readonly',
          ],
          '#value' => '',
        ],
        'remote_add_http' => [
          '#prefix' => '<p>' . t('<strong>Or,</strong> if you do not have <a href="!instructions">SSH keys set up on git.drupalcode.org</a>:', [
            '!instructions' => url($versioncontrol_gitlab_url . '/-/profile/keys', ['external' => TRUE]),
          ]) . '</p>',
          '#type' => 'textarea',
          '#title' => t('Add & fetch this issue fork’s repository'),
          '#rows' => 2,
          '#resizable' => FALSE,
          '#attributes' => [
            'class' => ['remote-add-http', 'drupalorg-copy'],
            'readonly' => 'readonly',
          ],
          '#value' => '',
        ],
        '#suffix' => '</details>',
      ],
      'branches' => [
        '#prefix' => '<ul class="branches">',
        '#suffix' => '</ul>',
      ],
      'new_branch' => [],
    ];

    // Get the likely branch to start from.
    if ($dev_release_node = $this->getDevRelease()) {
      $issue_branch = entity_metadata_wrapper('node', $dev_release_node)->field_release_vcs_label->value();
      $output['#attached']['js'][0]['data']['drupalorgIssueBranch'] = $issue_branch;
    }

    if (user_is_logged_in() && (versioncontrol_gitlab_get_user_id($GLOBALS['user']))) {
      // Get push access form.
      $output['header']['access'] = drupal_get_form('drupalorg_issue_fork_access_form', $this);

      // New branch link.
      $uri = $this->uri();
      $uri['path'] .= '/-/branches/new';
      $uri['options']['query']['branch_name'] = $this->getIssueWrapper()->getIdentifier() . '-';
      if (isset($issue_branch)) {
        $uri['options']['query']['ref'] = $issue_branch;
      }
      $output['new_branch'] = [
        '#prefix' => '<div class="new-branch">' . l(t('Create new branch'), $uri['path'], $uri['options']),
        '#type' => 'textfield',
        '#title' => t('Or push your current local branch from your Git clone'),
        '#value' => 'git push --set-upstream ' . $this->label() . ' HEAD',
        '#attributes' => [
          'class' => ['drupalorg-copy'],
          'readonly' => 'readonly',
        ],
        '#suffix' => '</div>',
      ];
    }

    // Get branches used in the fork.
    $compare_image = theme('image', [
      'path' => drupal_get_path('module', 'drupalorg') . '/images/compare-black.svg',
      'alt' => t('Compare'),
      'title' => t('Compare'),
      'width' => '20',
      'height' => '20',
    ]);
    $branch_result = $this->getBranches();
    foreach ($branch_result as $row) {
      foreach ($row->merge_requests as $iid => $info) {
        $output['#attached']['js'][0]['data']['drupalorgBranchData'][$row->branch]['!' . $iid] = [
          'initialCommit' => $info['initial_commit'],
          'initialCid' => $info['initial_cid'],
        ];
      }
      $branch_name_sanitized = check_plain($row->branch);
      $branch_output = [
        '#prefix' => '<li data-branch="' . $branch_name_sanitized . '">',
        'link' => [
          '#markup' => '<strong><a class="link" title="' . t('View branch in GitLab') . '">' . $branch_name_sanitized . '</a></strong> ',
        ],
        'operations' => [],
        'help' => [
          '#type' => 'textfield',
          '#title' => t('Check out this branch for the first time'),
          '#value' => 'git checkout -b ' . escapeshellarg($row->branch) . ' --track ' . $this->label() . '/' . escapeshellarg($row->branch),
          '#attributes' => [
            'class' => ['drupalorg-copy'],
            'readonly' => 'readonly',
          ],
        ],
        'existing_branch' => [
          '#type' => 'textfield',
          '#title' => t('Check out existing branch, if you already have it locally'),
          '#value' => 'git checkout ' . escapeshellarg($row->branch),
          '#attributes' => [
            'class' => ['drupalorg-copy'],
            'readonly' => 'readonly',
          ],
        ],
        '#suffix' => '</li>',
      ];
      // Merge requests for the branch.
      if (empty($row->merge_requests)) {
        $branch_output['operations']['compare'] = [
          '#prefix' => '<a class="compare" title="Compare branches in the issue fork">',
          '#markup' => $compare_image . t('compare'),
          '#suffix' => '</a> ',
        ];
      }
      foreach ($row->merge_requests as $iid => $info) {
        $merge_request_url = $this->mergeRequestUrl($iid);
        $branch_output['operations'][] = [
          '#prefix' => !empty($branch_output['operations']) ? ' / ' : '',
          'compare' => [
            '#markup' => l($compare_image . t('changes'), $merge_request_url . '/diffs', [
              'attributes' => [
                'class' => ['compare'],
                'title' => t('Show code changes for the merge request'),
              ],
              'html' => TRUE,
              'external' => TRUE,
            ]) . ', ' . l('<span class="description">' . t('plain diff') . '</span>', $merge_request_url . '.diff', [
              'attributes' => [
                'title' => t('Can be applied as a patch'),
              ],
              'html' => TRUE,
              'external' => TRUE,
            ]) . ' ',
          ],
          'mr' => [
            '#markup' => l(t('<strong><abbr title="Merge Request">MR</abbr> !@iid</strong>', [
              '@iid' => $iid,
            ]), $merge_request_url, [
              'attributes' => ['class' => ['merge-request']],
              'html' => TRUE,
              'external' => TRUE,
            ]),
          ],
        ];
      }

      if (user_access('edit any project_issue content')) {
        $toggle_text = ($row->status == 'hidden') ? t('show') : t('hide');
        $new_status = ($row->status == 'hidden') ? 'active' : 'hidden';
        $branch_output['operations']['toggle'] = [
          '#prefix' => '<small><a class="toggle-branch" title="' . t('@toggle branch for everyone', [
            '@toggle' => ucfirst($toggle_text),
          ]) . '" href="' . url('drupalorg_toggle_branch_visibility/' . $this->gitlab_project_id . '/' . urlencode($row->branch) . '/' . $new_status, [
            'query' => [
              'token' => drupal_get_token('drupalorg-toggle-branch-' . $this->gitlab_project_id . '-' . $row->branch . '-' . $new_status),
              'destination' => current_path(),
            ]
          ]) . '">',
          '#markup' => t('@toggle branch', [
            '@toggle' => ucfirst($toggle_text),
          ]),
          '#suffix' => '</a></small> ',
        ];
      }
      $output['branches'][$row->status][] = $branch_output;
    }

    if (!empty($output['branches']['hidden'])) {
      $output['branches']['hidden']['#prefix'] = '<details><summary>' . format_plural(count($output['branches']['hidden']), '1 hidden branch', '@count hidden branches') . '</summary>';
      $output['branches']['hidden']['#suffix'] = '</details>';
      $output['branches']['hidden']['#weight'] = 1;
    }

    // Add commenter data.
    $commenter_uids = [];
    foreach (drupalorg_get_comments($this->getIssueWrapper()->raw()) as $comment) {
      $commenter_uids[$comment->uid] = TRUE;
    }
    if (!empty($commenter_uids)) {
      $result = db_query('SELECT uid, git_username, picture, name FROM {users} u WHERE u.uid IN (:uids) AND git_username IS NOT NULL', [':uids' => array_keys($commenter_uids)]);
      foreach ($result as $row) {
        $output['#attached']['js'][0]['data']['drupalorgGitUsers'][$row->git_username] = [
          'url' => url('user/' . $row->uid),
          'picture' => drupalorg_comment_picture($row),
        ];
      }
    }
    $output['#attached']['js'][0]['data']['drupalorgGitUsers'][''] = [
      'picture' => drupalorg_comment_picture((object) ['uid' => 0, 'name' => t('Unmatched user'), 'picture' => 0]),
    ];

    return $output;
  }

  /**
   * Get branches for the issue fork.
   */
  public function getBranches() {
    if (is_null($this->branches)) {
      $this->branches = [];
      $result = db_query("SELECT branch, status, merge_request_iid, merge_request_initial_commit, merge_request_initial_cid FROM {drupalorg_issue_fork_branches} WHERE gitlab_project_id = :gitlab_project_id ORDER BY last_activity DESC", [
        ':gitlab_project_id' => $this->gitlab_project_id,
      ]);
      foreach ($result as $row) {
        if (!isset($this->branches[$row->branch])) {
          $this->branches[$row->branch] = new stdClass();
          $this->branches[$row->branch]->branch = $row->branch;
          $this->branches[$row->branch]->status = $row->status;
          $this->branches[$row->branch]->merge_requests = [];
        }
        elseif ($this->branches[$row->branch]->status !== 'active') {
          // Keep “active” status for the row if already set.
          $this->branches[$row->branch]->status = $row->status;
        }
        if (!empty($row->merge_request_iid)) {
          $this->branches[$row->branch]->merge_requests[$row->merge_request_iid] = [
            'initial_commit' => $row->merge_request_initial_commit,
            'initial_cid' => $row->merge_request_initial_cid,
          ];
        }
      }
    }

    return $this->branches;
  }

  /**
   * Get the URL for a merge request.
   *
   * @param int $iid
   *   The merge request IID.
   *
   * @return string
   *   The URL for the merge request.
   */
  public function mergeRequestUrl($iid) {
    if (is_null($this->project_gitlab_url)) {
      $this->project_gitlab_url = versioncontrol_project_repository_load($this->project_nid)->getUrlHandler()->getRepositoryViewUrl();
    }

    return $this->project_gitlab_url . '/-/merge_requests/' . $iid;
  }

  /**
   * Check if a merge request can be merged.
   *
   * @param int $iid
   *   The merge request IID.
   * @param int $poll_delay
   *   Number of microseconds to wait between polling requests.
   *
   * @return bool
   *   If the merge request can currently be merged.
   */
  public function mergeRequestCanBeMerged($iid, $poll_delay = 10000) {
    /* todo use when https://github.com/GitLabPHP/Client/pull/588 lands, and watch out for https://github.com/GitLabPHP/Client/issues/573
    $client = versioncontrol_gitlab_get_client()->api('merge_requests');
    $project_repository = versioncontrol_project_repository_load($this->project_nid);
    $client->all($project_repository->namespace . '/' . $project_repository->name, [
      'iids' => [(int) $iid],
      'with_merge_status_recheck' => TRUE,
    ]);
    */
    $project_repository = versioncontrol_project_repository_load($this->project_nid);
    $result = versioncontrol_gitlab_get_client()->getHttpClient()->get('projects/' . urlencode($project_repository->namespace . '/' . $project_repository->name) . '/merge_requests?' . http_build_query([
      'iids[]' => $iid,
      'with_merge_status_recheck' => TRUE,
    ]));
    timer_start('merge_status_check');
    while (in_array(($merge_status = $this->fetchMergeRequestInfo($iid)['merge_status']), ['unchecked', 'checking']) && timer_read('merge_status_check') < 60*1000) {
      usleep($poll_delay);
    }
    return $merge_status === 'can_be_merged';
  }

  /**
   * Merge a merge request.
   *
   * @param int $iid
   *   The merge request IID.
   * @param string $message
   *   The commit message to use.
   *
   * @return bool
   *   If the merge was successful.
   */
  public function mergeMergeRequest($iid, $message) {
    try {
      if (!$this->isMember($GLOBALS['user'])) {
        $this->addMember($GLOBALS['user']);
      }
      $gitlab_api = versioncontrol_gitlab_get_client($GLOBALS['user']->git_username)->api('merge_requests');
      $project_repository = versioncontrol_project_repository_load($this->project_nid);
      // Rebase and wait up to 10s for completion.
      $gitlab_api->rebase($project_repository->namespace . '/' . $project_repository->name, $iid, [
        'skip_ci' => TRUE,
      ]);
      $tries = 0;
      while (($merge_request_info = $this->fetchMergeRequestInfo($iid, ['include_rebase_in_progress' => TRUE])) && $merge_request_info['rebase_in_progress']) {
        $tries += 1;
        if ($tries > 100) {
          drupal_set_message(t('Rebasing for merge timed out.'), 'error');
          return FALSE;
        }
        usleep(100);
      }
      if ($tries > 0) {
        usleep(100);
      }
      if (!empty($merge_request_info['merge_error'])) {
        drupal_set_message(check_plain($merge_request_info['merge_error']), 'error');
        return FALSE;
      }
      // Wait for merge status rechecking.
      if (!$this->mergeRequestCanBeMerged($iid)) {
        drupal_set_message(t('Merge request can not be merged.'), 'error');
        return FALSE;
      }
      // Merge.
      $gitlab_api->merge($project_repository->namespace . '/' . $project_repository->name, $iid, [
        'squash' => TRUE,
        'should_remove_source_branch' => FALSE,
        'squash_commit_message' => $message,
      ]);
      return TRUE;
    }
    catch (Exception $e) {
      $uri = $this->uri();
      watchdog('drupalorg_issue_fork', 'Exception in merging !@iid for !issue: %msg', [
        '@iid' => $iid,
        '!issue' => l($this->label(), $uri['path'], $uri['options']),
        '%msg' => $e->getMessage(),
      ], WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * A snippet of HTML confirming access.
   *
   * @return string
   *   HTML string.
   */
  public static function hasPushAccessHtml() {
    return '<span class="push-access description">' . t('✓ You have push access') . '</span>';
  }

  /**
   * Get a merge request IID by branch name.
   *
   * @param string $branch
   *   A branch name.
   * @param string $target_branch
   *   The target branch name.
   *
   * @return int
   *   The associatied merge request’s IID.
   */
  public function getMergeRequestIID($branch, $target_branch) {
    return db_query_range('SELECT merge_request_iid FROM {drupalorg_issue_fork_branches} WHERE gitlab_project_id = :gitlab_project_id AND branch = :branch ORDER BY merge_request_target_branch = :target_branch DESC', 0, 1, [
      ':gitlab_project_id' => $this->gitlab_project_id,
      ':branch' => $branch,
      ':target_branch' => $target_branch,
    ])->fetchField();
  }

  /**
   * Load issue forks for an issue.
   *
   * @param EntityDrupalWrapper $node_wrapper
   *   An entity wrapper for an issue node.
   *
   * @return array
   *   An array of DrupalorgIssueFork objects.
   */
  public static function loadIssueForks(EntityDrupalWrapper $issue_wrapper) {
    static $issue_forks = [];

    if (!isset($issue_forks[$issue_wrapper->getIdentifier()])) {
      $issue_forks[$issue_wrapper->getIdentifier()] = entity_load('drupalorg_issue_fork', db_query('SELECT gitlab_project_id FROM {drupalorg_issue_forks} WHERE nid = :nid', [
        ':nid' => $issue_wrapper->getIdentifier(),
      ])->fetchCol());
    }

    return $issue_forks[$issue_wrapper->getIdentifier()];
  }

  /**
   * Get the EntityDrupalWrapper for this fork’s issue.
   *
   * @return EntityDrupalWrapper
   *   The EntityDrupalWrapper for this fork’s issue.
   */
  public function getIssueWrapper() {
    static $issue_wrapper;

    if (is_null($issue_wrapper)) {
      $issue_wrapper = entity_metadata_wrapper('node', node_load($this->nid));
    }

    return $issue_wrapper;
  }

  /**
   * Get the dev release node for the branch associated with the issue.
   *
   * @return object|null
   *   A release node, or NULL if one can not be found.
   */
  public function getDevRelease() {
    return drupalorg_find_dynamic_release($this->getIssueWrapper()->raw());
  }

  /**
   * Fetch project information from GitLab.
   *
   * @return array
   *   See https://docs.gitlab.com/ee/api/projects.html#get-single-project.
   */
  private function fetchProjectInfo() {
    return versioncontrol_gitlab_get_client()->api('projects')->show($this->gitlab_project_id);
  }

  /**
   * Fetch merge request information from GitLab.
   *
   * @param int $iid
   *   The merge request IID.
   * @param array $parameters
   *   Query paramters to pass to MergeRequests::show().
   *
   * @return array
   *   See https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr.
   */
  private function fetchMergeRequestInfo($iid, array $parameters = []) {
    $client = versioncontrol_gitlab_get_client()->api('merge_requests');
    $project_repository = versioncontrol_project_repository_load($this->project_nid);
    return $client->show($project_repository->namespace . '/' . $project_repository->name, $iid, $parameters);
  }

  /**
   * Check if a user has push access.
   *
   * @param object $account
   *   A Drupal user account object.
   *
   * @return bool
   *   True if the user is a member of the project.
   */
  private function isMember(stdClass $account) {
    $members = versioncontrol_gitlab_get_client()->api('projects')->allMembers($this->gitlab_project_id);
    if (!empty($members)) {
      foreach ($members as $member) {
        if ($member['username'] == $account->git_username) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Builds a Tugboat preview.
   *
   * @param object $payload
   *   The payload from the merge request event.
   */
  public function buildTugboatPreview($payload) {
    try {
      $tugboat_preview = TugboatPreview::fromGitLabPayload($payload);
    }
    catch (UnsupportedMergeRequestEventException $e) {
      return;
    }
    catch (ProjectDoesNotSupportTugboatException $e) {
      return;
    }
    catch (Exception $e) {
      watchdog('drupalorg_merge_request', 'Could not construct a TugboatPreview object. Error is !error', [
        '!error' => $e->getMessage(),
      ], WATCHDOG_ERROR);
      return;
    }

    try {
      /** @var \Psr\Http\Message\ResponseInterface */
      $response = $tugboat_preview->build();
      watchdog('drupalorg_merge_request', 'Triggered a preview at Tugboat with payload !payload. Response was !response', [
        '!payload' => json_encode($tugboat_preview->toPayload()),
        '!response' => $response->getBody()->__toString(),
      ], WATCHDOG_INFO);
    }
    catch (Exception $e) {
      watchdog('drupalorg_merge_request', 'Tugboat failed to build a preview with payload !payload. Error was !error', [
        '!payload' => json_encode($tugboat_preview->toPayload()),
        '!error' => $e->getMessage(),
      ], WATCHDOG_ERROR);
    }
  }

  /**
   * Returns Tugboat previews for the current issue fork.
   *
   * @return array
   *   An array of arrays where each item has the following structure:
   * <code>
   * [
   *   'id' => 'foo',
   *   'branch' => 'branch-name',
   *   'state' => 'building'
   *   'suspended' => 'building'
   *   'url' => 'http://foo-bar.baz',
   * ]
   * </code>
   */
  public function getTugboatPreviews() {
    $previews = [];

    $repository_data = versioncontrol_project_repository_load($this->project_nid);
    $repository_name = $repository_data->namespace . '/' . $repository_data->name;

    try {
      foreach (TugboatApi::getPreviewsFor($repository_name) as $preview) {
        if (!empty($preview->provider_ref->source) && ($preview->provider_ref->source->path == $this->label())) {
          $previews[] = [
            'id' => $preview->id,
            'branch' => $preview->provider_ref->source_branch,
            'state' => $preview->state,
            'suspended' => !empty($preview->suspended) ? $preview->suspended : '',
            'url' => !empty($preview->url) ? $preview->url : '',
          ];
        }
      }
    }
    catch (Exception $e) {
      watchdog('drupalorg_issue_fork', 'Failed to fetch Tugboat previews for issue fork @issue_fork. Error was @error', [
        '@issue_fork' => $this->label(),
        '@error' => $e->getMessage(),
      ], WATCHDOG_ERROR);
    }

    return $previews;
  }

}

/**
 * Form for getting access to an issue fork.
 */
function drupalorg_issue_fork_access_form(array $form, array $form_state, \DrupalorgIssueFork $issue_fork) {
  // An issue may have multiple forks if it has moved from one project to
  // another, potentially a fork for each project. Use a unique ID.
  $id = 'drupalorg-issue-fork-access-form-' . $issue_fork->gitlab_project_id;
  $form['#id'] = $id;
  $form['#attributes'] = ['class' => ['drupalorg-issue-fork-access']];

  $form['actions'] = [
    '#type' => 'actions',
    'get_access' => [
      '#type' => 'submit',
      '#value' => t('Checking access…'),
      '#attributes' => ['disabled' => 'disabled'],
      '#ajax' => [
        'callback' => 'drupalorg_issue_fork_access_form_ajax',
        'progress' => ['type' => 'throbber', 'message' => t('Getting…')],
        'wrapper' => $id,
      ],
    ],
  ];

  return $form;
}

/**
 * Form submit callback for drupalorg_issue_fork_access_form().
 */
function drupalorg_issue_fork_access_form_submit(array $form, array &$form_state) {
  $issue_fork = $form_state['build_info']['args'][0];

  try {
    $issue_fork->addMember($GLOBALS['user']);
    $form_state['drupalorg_has_access'] = TRUE;
  }
  catch (Exception $e) {
    $uri = $issue_fork->uri();
    watchdog('drupalorg_issue_fork', 'Exception in adding member to !issue: %msg', [
      '!issue' => l($issue_fork->label(), $uri['path'], $uri['options']),
      '%msg' => $e->getMessage(),
    ], WATCHDOG_ERROR);
    drupal_set_message(t('Getting push access failed!'), 'error');
  }
}

/**
 * #ajax callback for drupalorg_issue_fork_access_form().
 */
function drupalorg_issue_fork_access_form_ajax(array $form, array $form_state) {
  if ($form_state['drupalorg_has_access']) {
    return DrupalorgIssueFork::hasPushAccessHtml();
  }
  else {
    // Let reattaching the form re-trigger behaviors, any errors will be shown.
    return $form;
  }
}
