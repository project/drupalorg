<?php

/**
 * Class ProjectDoesNotSupportTugboatException
 *
 * Represents the exception when a project does not support Tugboat.
 */
class ProjectDoesNotSupportTugboatException extends Exception {}
