<?php

/**
 * Class ProjectDoesNotSupportTugboatException
 *
 * Represents the exception when a merge request event is unsupported to build a Tugboat preview.
 */
class UnsupportedMergeRequestEventException extends Exception {}
