<?php

use GuzzleHttp\Client;

/**
 * Class Tugboat
 *
 * Provides an interface for the Tugboat API.
 */
class TugboatApi {

  /**
   * Get a Tugboat API client.
   *
   * @return \GuzzleHttp\Client
   *   A Guzzle client object.
   */
  public static function getClient() {
    static $client;

    if (is_null($client)) {
      $client = new Client([
        'base_uri' => variable_get('drupalorg_tugboat_api', 'https://api.tugboatqa.com'),
        'timeout' => variable_get('drupalorg_tugboat_timeout', 20.0),
        'headers' => [
          'Authorization' => 'Bearer ' . variable_get('tugboat_api_token'),
          'Accept' => 'application/json',
        ],
      ]);
    }

    return $client;
  }

  /**
   * Rebuilds a Tugboat preview.
   *
   * @param string $preview_id
   *   The preview identifier.
   *
   * @throws \Exception
   *   If there was an unsuccessful response from the Tugboat API.
   */
  public static function rebuildPreview($preview_id) {
    $response = self::getClient()->post('/v3/previews/' . $preview_id . '/rebuild');
    $response_body = $response->getBody()->__toString();

    if ($response->getStatusCode() !== 202) {
      throw new Exception($response_body);
    }
  }

  /**
   * Returns the build log for a Tugboat preview.
   *
   * @param string $preview_id
   *   The preview identifier.
   *
   * @return array
   *   An array of objects with the following structure:
   * <code>
   * stdClass::__set_state(array(
   *   'timestamp' => '2020-10-12T20:31:46.235Z',
   *   'level' => 'info',
   *   'message' => 'Building preview: foo (mr72)',
   *   'job' => 'bar',
   *   'preview' => 'foo',
   *   'id' => 'baz',
   * ))
   * </code>
   *
   * @throws \Exception
   *   If there was an unsuccessful response from the Tugboat API.
   */
  public static function getPreviewLog($preview_id) {
    $response = self::getClient()->get('/v3/previews/' . $preview_id . '/log');
    $response_body = $response->getBody();
    if ($response->getStatusCode() !== 200) {
      throw new Exception($response_body);
    }

    return json_decode($response_body);
  }

  /**
   * Returns a tugboat repository ID by its name.
   *
   * @param string $repository_name
   *   The project repository, such as project/drupal.
   *
   * @return string
   *   The repository ID or FALSE if not found.
   */
  public static function getRepositoryId($repository_name) {
    return db_query_range('SELECT repo_id FROM {drupalorg_tugboat_repos} WHERE name = :repository_name', 0, 1, [':repository_name' => $repository_name])->fetchField();
  }

  /**
   * Registers a repository.
   *
   * @param string $namespace
   *   The repository namespace, like project.
   * @param string $name
   *   The repository name, like drupal.
   *
   * @return string
   *   The response from Tugboat.
   *
   * @throws \Exception
   *   If there was an error.
   */
  public static function registerRepository($namespace, $name) {
    $gitlab_token = variable_get('tugboat_gitlab_api_token');

    $body = [
      'project' => variable_get('tugboat_project_id'),
      'provider' => [
        'name' => 'gitlab',
        'address' => drupalorg_gitlab_url_with_htauth(),
      ],
      'repository' => [
        'group' => $namespace,
        'name' => $name,
      ],
      'auth' => [
        'token' => $gitlab_token,
      ],
      'autobuild' => FALSE,
      'autodelete' => TRUE,
      'autorebuild' => TRUE,
      'build_timeout' => 3600,
      'name' => $namespace . '/' . $name,
      'provider_comment' => FALSE,
      'provider_deployment' => TRUE,
      'provider_forks' => TRUE,
      'provider_status' => TRUE,
      'quota' => 0,
      'refresh_anchors' => TRUE,
      'refresh_day' => 7,
      'refresh_hour' => 0,
      'rebuild_orphaned' => FALSE,
      'rebuild_stale' => FALSE,
    ];

    $response = self::getClient()->request('POST', '/v3/repos', [
      'headers' => ['Content-Type' => 'application/json'],
      'body' => json_encode($body),
    ])->getBody()->__toString();

    $repository = json_decode($response);
    db_insert('drupalorg_tugboat_repos')
      ->fields([
        'name' => $repository->name,
        'repo_id' => $repository->id,
      ])
      ->execute();

    return $response;
  }

  /**
   * Updates the provider authentication credentials for a Repository.
   *
   * @param string $repository_id
   *    The Tugboat ID for the repository to update.
   *
   * @return string
   *    The response from Tugboat.
   *
   * @throws \Exception
   *   If there was an error with the Tugboat API.
   */
  public static function updateRepositoryAuth($repository_id) {
    $gitlab_token = variable_get('tugboat_gitlab_api_token');

    $body = [
      'auth' => ['token' => $gitlab_token],
    ];
    $response = self::getClient()->request('PATCH', '/v3/repos/' . $repository_id . '/auth', [
      'headers' => ['Content-Type' => 'application/json'],
      'body' => json_encode($body),
    ])->getBody()->__toString();
    return $response;
  }

  /**
   * Returns all Tugboat repos in the Tugboat project.
   *
   * @return array
   *   The array of repositories.
   *
   * @throws \Exception
   *   If there was an error with the Tugboat API.
   */
  public static function getProjectRepositories() {
    $repos = [];
    $tugboat_project_id = variable_get('tugboat_project_id');

    $response = self::getClient()->get('/v3/projects/' . $tugboat_project_id . '/repos');
    foreach (json_decode($response->getBody()) as $repo) {
      $repos[] = $repo;
    }

    return $repos;
  }

  /**
   * Returns Tugboat previews for a given repository.
   *
   * @param string $repository_name
   *   The repository name, such as project/drupal
   *
   * @return array
   *   The array of previews.
   *
   * @throws \Exception
   *   If there was an error with the Tugboat API.
   */
  public static function getPreviewsFor($repository_name) {
    $previews = [];

    if ($repository_id = self::getRepositoryId($repository_name)) {
      $response = self::getClient()->get('/v3/repos/' . $repository_id . '/previews');
      foreach (json_decode($response->getBody()) as $preview) {
        $previews[] = $preview;
      }
    }

    return $previews;
  }

}
