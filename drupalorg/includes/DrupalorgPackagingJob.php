<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Yaml\Yaml;

/**
 * @file
 * Entity class definition for DrupalorgPackagingJob class.
 */
class DrupalorgPackagingJob extends Entity {

  /**
   * @var int Unique ID for this Packaging data entity.
   */
  public $job_id;

  /**
   * @var int Release node ID.
   */
  public $release_nid;

  /**
   * @var string Related URL of the jenkins job.
   */
  public $jenkins_job_url;

  /**
   * @var string Status of the packaging pipeline job
   */
  public $status = DRUPALORG_JOB_STATUS_QUEUE;

  /**
   * @var int Timestamp when this record was updated.
   */
  public $updated = NULL;

  /**
   * @var int Timestamp when this record was created.
   */
  public $created = NULL;

  /**
   * @var int Boolean indicating whether or not this is a security release (1)
   */
  public $is_security = 0;

  /**
   * @var int Project node ID.
   */
  public $project_nid;

  /**
   * @var string project shortname
   */
  public $project_machine_name = '';

  /**
   * @var string url to the project in git.
   */
  public $project_git_url = '';

  /**
   * @var string Branch or tag label.
   */
  public $reference = '';

  /**
   * @var string Extended build details, usually failure explanation.
   */
  public $release_build_type = '';

  /**
   * @var string SHA1 of the commit to be packaged.
   */
  public $commit_id = '';

  /**
   * @var string SHA1 of last commit that was packaged.
   */
  public $last_commit_id = '';

  /**
   * @var \GuzzleHttp\Client
   */
  public $jenkins_api_client;

  /**
   * @var \Symfony\Component\Yaml\Yaml
   */
  public $yaml_parser;

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    try {
      parent::__construct($values, 'drupalorg_packaging_job');
    }
    catch (Exception $e) {
      // Thats a really terrible use of an exception.
    }

    $this->getJenkinsClient();
    $this->getYamlParser();
  }

  /**
   * Prepare for serializiation.
   */
  public function __sleep() {
    $vars = parent::__sleep();
    unset($vars['jenkins_api_client'], $vars['yaml_parser']);
    return drupal_map_assoc(array_keys($vars));
  }

  /**
   * Prepare for unserializiation.
   */
  public function __wakeup() {
    $this->getJenkinsClient();
    $this->getYamlParser();
    $vars = parent::__wakeup();
  }


  /**
   * Establishes the guzzle client to use to communicate with Jenkins.
   *
   * @param null $url
   *   The url of the job, or just dispatcher.
   */
  protected function getJenkinsClient() {
    $url = variable_get('drupalorg_jenkins_server');
    $url_parts = 'buildByToken/build?job=package_release_pipeline';
    $this->jenkins_api_client = new Client([
      // Base URI is used with relative requests
      'base_uri' => $url,
      // You can set any number of default request options.
      'timeout' => 90.0,
      'headers' => ['Content-Type' => 'application/json', "Accept" => "application/json"],
    ]);
  }

  /**
   *  Creates a Yaml parser that the job can use for reading in templates
   *  and for dumping the build into yaml to send.
   */
  protected function getYamlParser() {
    $this->yaml_parser = new Yaml();
  }

  /**
   * Load an DrupalorgPackagingJob entity.
   *
   * @param int $job_id ID of the Packaging job to load.
   *
   * @return \DrupalorgPackagingJob
   */
  public static function load($job_id) {
    return entity_load_single('drupalorg_packaging_job', $job_id);
  }

  /**
   * Save a DrupalorgPackagingJob entity.
   *
   * @param bool $complete
   *   TRUE to update the issue & send email notifications as needed.
   * @param bool $preserve_timestamp
   *   TRUE to keep the existing timestamp of the job.
   *
   * @return
   * @throws \Exception
   */
  public function save($complete = FALSE, $preserve_timestamp = FALSE) {
    if (!$preserve_timestamp) {
      $this->updated = REQUEST_TIME;
    }
    // New test.
    if (empty($this->created)) {
      $this->created = REQUEST_TIME;

    }

    return parent::save();
  }

  /**
   * Takes a queued request and pushes it upstream to the api server.
   *
   * @return bool
   */
  public function requestJob() {

    // Get the jenkins job url for this environment
    $jenkins_server_url = variable_get('drupalorg_jenkins_url', "http://ctrl1.drupal.bak:8080");
    $pipeline_job_token = variable_get('drupalorg_pipeline_job_token');

    $query = "job=package_release_pipeline&token=${pipeline_job_token}&job_id={$this->job_id}&release_nid={$this->release_nid}&project_nid={$this->project_nid}&project_machine_name={$this->project_machine_name}&git_url={$this->project_git_url}&reference={$this->reference}&release_build_type={$this->release_build_type}&commit_id={$this->commit_id}&last_commit_id={$this->last_commit_id}";

    try {
      $response = $this->jenkins_api_client->post($jenkins_server_url . '/buildByToken/buildWithParameters', [
        'query' => $query,
      ]);
      $this->status = DRUPALORG_JOB_STATUS_SENT;
      $this->save();

      return TRUE;
    }
    catch (RequestException $e) {
      watchdog('drupalorg_packaging', 'Packaging job submission failure: @error', array('@error' => $e->getMessage()), WATCHDOG_WARNING);
      return FALSE;
    }
    catch (Exception $e) {
      watchdog('drupalorg_packaging', 'Packaging job save failure: @error', array('@error' => $e->getMessage()), WATCHDOG_WARNING);
      return FALSE;
    }
  }

  public function queueJob(){
    $queue = DrupalQueue::get('drupalorg_packaging_pipeline');
    $queue->createItem(['job_id' => $this->job_id]);
  }

}
