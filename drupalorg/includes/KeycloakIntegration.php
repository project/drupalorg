<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Stevenmaguire\OAuth2\Client\Provider\Keycloak;

/**
 * Class KeycloakIntegration
 *
 * Provides an interface for keycloak integration.
 */
class KeycloakIntegration {

  const EXECUTE_TIME_LIMIT_SECONDS = 120;
  private $waitSeconds = 15;

  /**
   * @var \Stevenmaguire\OAuth2\Client\Provider\Keycloak
   */
  private $authProvider;

  /**
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * @var string
   */
  private $oauth2Token;

  public function __construct() {
    $this->authProvider();
    $this->httpClient();
  }

  /**
   * Construct a keycloak auth provider.
   *
   * @return \GuzzleHttp\Client
   */
  private function httpClient() {
    $this->httpClient = new Client($this->guzzleConfig());

  }

  private function guzzleConfig() {
    return variable_get('drupalorg_keycloak_guzzle_config', [
      'base_uri' => 'http://keycloak:8080/auth/',
      RequestOptions::TIMEOUT => 20.0,
      RequestOptions::HTTP_ERRORS => FALSE,
    ]);
  }

  private function authOptions() {
    return variable_get('drupalorg_keycloak_config', [
      'authServerUrl' => 'http://keycloak:8080/auth/',
      'realm' => 'main',
      'clientId' => 'drupalorg',
      'clientSecret' => 'sup3rs3cr3t!',
    ]);
  }

  /**
   * Construct a keycloak auth provider.
   */
  private function authProvider() {
    $authentication_collaborators = [
      'timeout' => $this->guzzleConfig()['timeout'],
    ];
    $this->authProvider = new Keycloak($this->authOptions(), $authentication_collaborators);
  }

  private function authenticate() {
    $this->oauth2Token = $this->authProvider->getAccessToken(
      'client_credentials', $this->authOptions()
    )->getToken();
  }

  /**
   * @param $uri
   * @param $method
   * @param array $body
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  private function invoke($uri, $method = 'GET', array $body = []) {
    $request = new Request(
      $method,
      $uri,
      [
        'Authorization' => sprintf('Bearer %s', $this->oauth2Token),
        'Content-Type' => 'application/json',
      ],
      json_encode($body)
    );
    $response = $this->httpClient->send($request, [
      RequestOptions::HTTP_ERRORS => FALSE,
    ]);
    if ($response->getStatusCode() === 401) {
      $this->authenticate();
      return $this->invoke($uri, $method, $body);
    }
    return $response;
  }

  public function onUserChange($uid) {
    $uuid = $this->getKeycloakId($uid);
    if ($uuid) {
      try {
        $this->authenticate();
        $result = $this->updateUser($uid, $uuid);
        if ($result === FALSE) {
          watchdog('keycloak', 'User (@user) failed syncing their user profile.', [
            '@user' => $uid,
          ], WATCHDOG_ERROR);
        }
        else {
          watchdog('keycloak', 'User (@user) successfully synced their user profile.', [
            '@user' => $uid,
          ]);
        }
      }
      catch (Exception $e) {
        watchdog('keycloak', 'User (@uid) @class exception in syncing user profile: %message', [
          '@uid' => $uid,
          '@class' => get_class($e),
          '%message' => $e->getMessage(),
        ], WATCHDOG_ERROR);
        DrupalQueue::get('drupalorg_keycloak')->createItem($uid);
      }
    }
    else {
      watchdog('keycloak', 'User (@user) does not exist to sync their user profile.', [
        '@user' => $uid,
      ]);
    }
  }

  public function getUser($uuid) {
    $this->authenticate();
    $result = $this->invoke(sprintf('admin/realms/%s/users/%s', $this->authOptions()['realm'], $uuid), 'GET');
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if ($success) {
      return (string) $result->getBody();
    }
    return NULL;
  }

  public function getUserGroups($uuid) {
    $this->authenticate();
    $result = $this->invoke(sprintf('admin/realms/%s/users/%s/groups', $this->authOptions()['realm'], $uuid), 'GET');
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if ($success) {
      return (string) $result->getBody();
    }
  }

  public function integrityCheckUser($uid) {
    $uuid = $this->getKeycloakId($uid);
    $keycloak_user = $this->getUser($uuid);
    $kc_record = json_decode($keycloak_user, TRUE);
    $kc_groups = json_decode($this->getUserGroups($uuid), TRUE);
    $user_properties = [
      'username' => 'name',
      'enabled' => 'status',
      'email' => 'mail',
      'firstName' => 'firstName',
      'lastName' => 'lastName',
    ];
    $user_attributes = [
      'zoneinfo' => 'timezone',
      'sub' => 'uid',
    ];
    $query = db_select('users', 'u')
      ->condition('u.uid', $uid);
    $query->addField('f', 'filename', 'picture');
    $query->addField('fname', 'field_first_name_value', 'firstName');
    $query->addField('lname', 'field_last_name_value', 'lastName');
    $query->addField('spam', 'rid', 'spam');
    $query->fields('u', [
      'status',
      'name',
      'timezone',
      'uid',
      'mail',
      'created',
    ]);
    $query->leftJoin('file_managed', 'f', 'f.fid=u.picture');
    $query->leftJoin('field_data_field_first_name', 'fname', 'fname.entity_id=u.uid AND fname.bundle=\'user\' AND fname.deleted=0');
    $query->leftJoin('field_data_field_last_name', 'lname', 'lname.entity_id=u.uid AND lname.bundle=\'user\' AND lname.deleted=0');
    $query->leftJoin('users_roles', 'spam', 'spam.uid = u.uid AND spam.rid = :rid', [':rid' => (int) variable_get('drupalorg_crosssite_trusted_role', 36)]);
    $drupal_result = $query->execute()->fetchAssoc();
    foreach ($user_properties as $kc_name => $drupal_name) {
      if (drupal_strtolower($kc_record[$kc_name]) !== drupal_strtolower($drupal_result[$drupal_name])) {
        throw new \Exception(sprintf('uid: %s Keycloak(%s=%s) does not match Drupal(%s=%s)', $uid, $kc_name, $kc_record[$kc_name], $drupal_name, $drupal_result[$drupal_name]));
      }
    }
    $kc_confirmed = reset($kc_groups)['name'] === 'Confirmed';
    $drupal_confirmed = $drupal_result['spam'] == variable_get('drupalorg_crosssite_trusted_role', 36);
    if ($kc_confirmed !== $drupal_confirmed) {
      throw new \Exception(sprintf('uid: %s Confirmed does not match Keycloak (%s) Drupal (%s)', $uid, $kc_confirmed, $drupal_confirmed));
    }
    foreach ($user_attributes as $kc_name => $drupal_name) {
      if ($kc_record['attributes'][$kc_name][0] !== $drupal_result[$drupal_name]) {
        throw new \Exception(sprintf('uid: %s Keycloak(%s=%s) does not match Drupal(%s=%s)', $uid, $kc_name, $kc_record[$kc_name], $drupal_name, $drupal_result[$drupal_name]));
      }
    }
    if ($kc_record['createdTimestamp']/1000 !== (int) $drupal_result['created']) {
      throw new \Exception(sprintf('uid: %s Keycloak(legacy_created=%s) does not match Drupal(created=%s)', $uid, $kc_record['legacy_created'], $drupal_result['created']));
    }
    if (empty($kc_record['totp']) && array_intersect(drupalorg_2fa_required_roles(), array_keys($account->roles))) {
      // todo may also want to check $kc_record['requiredActions'] containing CONFIGURE_TOTP.
      throw new \Exception('uid: %s Keycloak does not require 2fa as expected');
    }
    if ($drupal_result['picture'] === '' && !array_key_exists('picture', $kc_record['attributes'])) {
      return;
    }
    if ($drupal_result['picture'] === '' && array_key_exists('picture', $kc_record['attributes'][0])) {
      throw new \Exception(sprintf('uid: %s Picture does not match Keycloak(%s) Drupal(%s)', $uid, $kc_record['picture'], $drupal_result['picture']));
    }
    if (!empty($drupal_result['picture']) && mb_strpos($kc_record['picture'][0], $drupal_result['picture']) !== FALSE && !\in_array($this->httpClient()
        ->get($kc_record['picture'])
        ->getStatusCode(), range(200, 299), TRUE)) {
      throw new \Exception(sprintf('uid: %s Picture does not match Keycloak(%s) Drupal(%s)', $uid, $kc_record['picture'][0], $drupal_result['picture']));
    }
    echo "No issue with user ($uid)\n";
  }

  private function getKeycloakId($uid) {
    return db_query("SELECT authname FROM {authmap} WHERE uid = :uid AND module = 'openid_connect_keycloak'", [
      ':uid' => $uid,
    ])->fetchField();
  }

  private function updateUser($user_id, $uuid) {
    $account = user_load($user_id);
    $style = 'grid-2-2x-square';
    $picture_path = '';
    if (is_numeric($account->picture)) {
      $picture_path = file_load($account->picture)->uri;
    }
    elseif (!empty($account->picture->uri)) {
      $picture_path = $account->picture->uri;
    }
    if (!empty($picture_path)) {
      $picture_path = drupalorg_ensure_image_style($style, $picture_path);
    }
    $kc_user = $this->getUser($uuid);
    if ($kc_user === NULL && $this->waitSeconds <= self::EXECUTE_TIME_LIMIT_SECONDS) {
      sleep($this->waitSeconds);
      $this->waitSeconds *= 2;
      return self::updateUser($user_id, $uuid);
    }
    $body = json_decode($kc_user, TRUE);
    unset($body['id'], $body['createdTimestamp'], $body['access']);
    $body['attributes'] = [
      'picture' => $picture_path,
      'sub' => $account->uid,
      'zoneinfo' => $account->timezone,
    ];
    $body['enabled'] = (bool) $account->status;
    if (!$body['totp'] && array_intersect(drupalorg_2fa_required_roles(), array_keys($account->roles))) {
      $body['requiredActions'][] = 'CONFIGURE_TOTP';
      $body['requiredActions'] = array_unique($body['requiredActions']);
    }
    $body = array_filter($body);
    $result = $this->invoke(sprintf('admin/realms/%s/users/%s', $this->authOptions()['realm'], $uuid), 'PUT', $body);
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if (!$success && $this->waitSeconds <= self::EXECUTE_TIME_LIMIT_SECONDS) {
      sleep($this->waitSeconds);
      $this->waitSeconds *= 2;
      return self::updateUser($user_id, $uuid);
    }
    // Per https://github.com/keycloak/keycloak/discussions/8552, this has
    // to be a separate REST call.
    $group_uuid = variable_get('drupalorg_keycloak_trusted_group', '2141a3e3-dd22-48bc-9023-11233f577d61');
    $method = isset($account->roles[variable_get('drupalorg_crosssite_trusted_role', 36)]) ? 'PUT' : 'DELETE';
    $result = $this->invoke(sprintf('admin/realms/%s/users/%s/groups/%s', $this->authOptions()['realm'], $uuid, $group_uuid), $method);
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if (!$success && $this->waitSeconds <= self::EXECUTE_TIME_LIMIT_SECONDS) {
      sleep($this->waitSeconds);
      $this->waitSeconds *= 2;
      return self::updateUser($user_id, $uuid);
    }

    return $success;
  }

  public function countUsers() {
    $this->authenticate();
    $result = $this->invoke(sprintf('admin/realms/%s/users/count', $this->authOptions()['realm']), 'GET');
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if ($success) {
      return (string) $result->getBody();
    }
    return NULL;
  }

  public function getUsers($query_parameters = []) {
    static $wait_seconds;
    if (!isset($wait_seconds)) {
      $wait_seconds = 2;
    }
    $this->authenticate();
    $uri = sprintf('admin/realms/%s/users', $this->authOptions()['realm']);
    if (!empty($query_parameters)) {
      $uri .= '?' . http_build_query($query_parameters);
    }
    try {
      $result = $this->invoke($uri, 'GET');
    }
    catch (ConnectException $e) {
      if ($wait_seconds <= self::EXECUTE_TIME_LIMIT_SECONDS) {
        sleep($wait_seconds);
        $wait_seconds *= 2;
        return $this->getUsers($query_parameters);
      }
      throw new \Exception(sprintf('User retrieval failed: "%s"', $e->getMessage()));
    }
    $result_code = $result->getStatusCode();
    $result_body = (string) $result->getBody();
    $success = \in_array($result_code, range(200, 299), TRUE);
    if ($success) {
      return $result_body;
    }
    throw new \Exception(sprintf('User retrieval failed (%d): "%s"', $result_code, $result_body));
  }

  public function getGroupMembers($group_uuid, $query_parameters = []) {
    $this->authenticate();
    $uri = sprintf('admin/realms/%s/groups/%s/members', $this->authOptions()['realm'], $group_uuid);
    $query_parameters += [
      'briefRepresentation' => TRUE,
    ];
    $uri .= '?' . http_build_query($query_parameters);
    $result = $this->invoke($uri, 'GET');
    $success = \in_array($result->getStatusCode(), range(200, 299), TRUE);
    if ($success) {
      return (string) $result->getBody();
    }
  }

}
