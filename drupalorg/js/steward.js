(function () {
  'use strict';

  window.addEventListener('DOMContentLoaded', () => {
    const formElement = document.getElementById('drupalorg-steward-pricing-form');
    const getFieldInt = (name) => {
      const element = formElement.querySelector('input[name=' + name + ']');
      // Set the value so that the browser does not warn about losing changed text when navigating.
      element.value = element.value;
      // Remove all non-digit characters.
      return parseInt(element.value.replace(/[^0-9]/g, ''), 10);
    }
    const updateEstimate = () => {
      const plan = + formElement.querySelector('input[name=plan]').checked;
      const domains = getFieldInt('domains');

      // Show the current plan pricing.
      formElement.querySelectorAll('.plan:not(.element-hidden)').forEach(function (planElement) {
        planElement.classList.add('element-hidden');
      });
      formElement.querySelector('.plan-' + plan).classList.remove('element-hidden');

      // Calculate the estimate.
      formElement.querySelector('.estimate strong').textContent = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format((
        domains * Drupal.settings.drupalorgSteward[plan].monthly +
        Math.max(0, getFieldInt('requests') - Drupal.settings.drupalorgSteward.requests_included * domains) / 10000 * Drupal.settings.drupalorgSteward[plan].overage
      ) / 100);
    }

    updateEstimate();
    formElement.addEventListener('change', updateEstimate);
  });
})();
