(function () {
  "use strict";

  Drupal.behaviors.drupalorgRiskCalc = {
    attach: function (context) {
      // Replace jQuery with DOM object.
      if (context.jquery !== undefined) {
        context = context.get(0);
      }

      const form = context.querySelector(".drupalorg-riskcalc");
      const nodeForm = form.closest("#sa-node-form");
      const result = document.createElement("p");
      let fieldInput;
      result.classList.add("field-name-field-sa-criticality");
      form.insertAdjacentElement("beforeend", result);

      if (!!nodeForm) {
        fieldInput = nodeForm.querySelector("#edit-field-sa-criticality-und-0-value");
        fieldInput.addEventListener("change", function (e) {
          fieldInput.value.split("/").forEach(function (part) {
            if (Drupal.settings.drupalorgRiskcalcValues[part] !== undefined) {
              form.querySelector("select[name='field_sa_criticality[riskcalc][0][riskcalc][" + part.split(":")[0] + "]']").value = part;
            }
          });
          form.dispatchEvent(new Event("change"));
        });
        fieldInput.dispatchEvent(new Event("change"));
      }

      form.addEventListener("change", function (e) {
        let incomplete = false;
        let score = 0;
        let values = [];
        let descriptions = [];
        let text;

        form.querySelectorAll("select").forEach(function (select) {
          if (select.value === "") {
            incomplete = true;
            return;
          }
          score += Drupal.settings.drupalorgRiskcalcValues[select.value].points;
          values.push(Drupal.settings.drupalorgRiskcalcValues[select.value].key);
          descriptions.push(Drupal.settings.drupalorgRiskcalcValues[select.value].description);
        });
        if (incomplete) {
          result.replaceChildren();
          return;
        }
        for (const threshold in Drupal.settings.drupalorgRiskcalcThresholds) {
          if (score <= threshold) {
            text = Drupal.settings.drupalorgRiskcalcThresholds[threshold];
            break;
          }
        }
        const link = document.createElement("a");
        link.classList.add(text.toLowerCase().replace(/ /g, "-"));
        link.setAttribute("title", descriptions.join("\n"));
        link.setAttribute("href", Drupal.settings.basePath + "security-team/risk-levels");
        link.innerHTML = Drupal.t("<strong>@text</strong> @score ∕ 25 @description", {
          "@text": text,
          "@score": score,
          "@description": values.join("/"),
        });
        result.replaceChildren(link);

        if (!!fieldInput) {
          fieldInput.value = values.join("/");
        }
      });
      form.dispatchEvent(new Event("change"));
    }
  };
})();
