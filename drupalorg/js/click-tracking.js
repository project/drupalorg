(function () {
  // Attach mousedown, keyup, touchstart events to document only and catch
  // clicks on all elements.
  document.body.addEventListener('mousedown', clickListener);
  document.body.addEventListener('keyup', clickListener);
  document.body.addEventListener('touchstart', clickListener);

  function clickListener(e) {
    // Catch the closest surrounding link of a clicked element.
    const link = e.target.closest('a, area');
    // Is the clicked URL external?
    if (typeof ga !== undefined && !!link && !((new RegExp("^(https?):\/\/" + window.location.host, 'i')).test(link.href)) && link.href.match(/^\w+:\/\//i)) {
      ga('gtm2.send', {
        'hitType': 'event',
        'eventCategory': 'Outbound links (' + e.type + ')',
        'eventAction': 'Click',
        'eventLabel': link.href,
        'transport': 'beacon'
      });
    }
  }
})();
