(function () {
    "use strict";
    Drupal.behaviors.drupalorgRegistrationprot = {
        attach: () => {
            const fpPromise = new Promise((resolve, reject) => {
                const script = document.createElement("script");
                script.onload = resolve;
                script.onerror = reject;
                script.async = true;
                script.src = Drupal.settings.drupalOrgFpjsSource;
                document.head.appendChild(script);
            })
            .then(() => FingerprintJS.load(Drupal.settings.drupalOrgFpjsParams))
            .then((fp) => fp.get())
            .then((result) => {
              var fpForm = document.forms['drupalorg-form-fp-form'];
              var submitButton = fpForm.querySelector('.form-submit');
              fpForm.querySelector('input[name=field_fingerprint]').value = result.visitorId;
              submitButton.click();
            });
        }
    };
})();
