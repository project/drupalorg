(function () {
  'use strict';

  const forks = {};
  const graphql = new Request(Drupal.settings.drupalorgGitlabUrl + '/api/graphql', {
    method: 'POST',
    credentials: 'include',
    headers: {'Content-Type': 'application/json'}
  });
  const dateFormat = new Intl.DateTimeFormat('en-GB', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
    timeZone: Drupal.settings.drupalorgTZ
  });
  const timeFormat = new Intl.DateTimeFormat('en-GB', {
    hour12: false,
    hour: 'numeric',
    minute: '2-digit',
    timeZone: Drupal.settings.drupalorgTZ
  });
  var commentList = null;

  if (document.readyState !== 'loading') {
    attachEvents();
  }
  else {
    document.addEventListener('DOMContentLoaded', attachEvents);
  }
  function attachEvents() {
    const commentsElement = document.querySelector('.comments');
    commentsElement.addEventListener('mouseover', updateDiscussionHighlight);
    commentsElement.addEventListener('focus', updateDiscussionHighlight, true);
  }
  function updateDiscussionHighlight(e) {
    const currentNote = e.target.closest('.in-discussion');
    for (let noteElement of document.querySelectorAll('.merge-request-activity li.highlighted')) {
      noteElement.classList.remove('highlighted');
    }
    if (currentNote !== null) {
      for (var className of currentNote.classList.values()) {
        if (className.startsWith('discussion-')) {
          for (let noteElement of document.querySelectorAll('.' + className)) {
            noteElement.classList.add('highlighted');
          }
          break;
        }
      }
    }
  }

  Drupal.behaviors.drupalorgIssueFork = {
    attach: context => {
      // Replace jQuery with DOM object.
      if (context.jquery !== undefined) {
        context = context.get(0);
      }

      if (context.classList !== undefined && context.classList.contains('push-access')) {
        // Push access was just granted.
        context.closest('.drupalorg-issue-fork').classList.add('have-push-access');
        return;
      }

      // Find issue forks.
      for (let issueFork of context.querySelectorAll('.drupalorg-issue-fork:not(.drupalorg-issue-fork-processed)')) {
        const url = new URL(issueFork.querySelector('.fork-link').getAttribute('href'));
        const path = url.pathname.replace(/^\//, '');
        const name = path.replace(/^issue\//, '');

        issueFork.classList.add('drupalorg-issue-fork-processed');

        forks[path] = {
          url: url,
          issueFork: issueFork,
          mergeRequests: [],
          notesAdded: 0,
          // Get project information.
          projectFetch: fetch(graphql, {
            body: JSON.stringify({
              query: `query projectFetch($path: ID!) {project(fullPath: $path) {
                httpUrlToRepo
                sshUrlToRepo
                repository {
                  rootRef
                }
              }}`,
              variables: {'path': path}
            })
          })
          .then(response => response.json())
        };
        // Set the “default” branch for the issue, falling back to a promise
        // for the fork’s default branch if the issue does not have a version.
        forks[path].issueBranch = Drupal.settings.drupalorgIssueBranch ?? forks[path].projectFetch.then(r => r.data.project.repository.rootRef);
        // Fill out project information.
        forks[path].projectFetch.then(r => {
          issueFork.querySelector('.remote-add-ssh').value = 'git remote add ' + name + ' ' + r.data.project.sshUrlToRepo + '\n' + 'git fetch ' + name;
          issueFork.querySelector('.remote-add-http').value = 'git remote add ' + name + ' ' + r.data.project.httpUrlToRepo + '\n' + 'git fetch ' + name;
        });

        // Check user’s access to the fork.
        fetch(Drupal.settings.basePath + 'drupalorg_fork_members/' + Drupal.settings.drupalorgIssueForks[name])
        .then(response => {
          return response.json();
        })
        .then(hasPushAccess => {
          const accessForm = issueFork.querySelector('form.drupalorg-issue-fork-access');
          if (accessForm !== null) {
            if (hasPushAccess) {
              accessForm.innerHTML = Drupal.t('✓ You have push access');
              issueFork.classList.add('have-push-access');
            }
            else {
              const button = accessForm.querySelector('input[name=op]');
              button.setAttribute('value', Drupal.t('Get push access'));
              button.removeAttribute('disabled');
            }
          }
        });

        if (issueFork.classList.contains('in-progress')) {
          window.setTimeout(waitForImport, 500, path);
        }
        else {
          fetchBranches(path);
        }

        // Find merge requests.
        for (let mergeRequest of issueFork.querySelectorAll('.merge-request')) {
          const url = new URL(mergeRequest.getAttribute('href'));
          const mergeId = url.pathname.match(/^\/(?<projectPath>.*)\/-\/merge_requests\/(?<iid>\d+)$/).groups;
          forks[path].mergeRequests[mergeId.iid] = {
            element: mergeRequest,
            class: 'no-status',
            discussions: {}
          }
          fetchMergeRequestActivity(mergeId.projectPath, mergeId.iid);
        }

        fetchTugboatPreviews(Drupal.settings.drupalorgIssueForks[name], issueFork);
      }
    }
  };

  function fetchBranches(path, page = 1) {
    const url = new URL(Drupal.settings.drupalorgGitlabUrl + '/api/v4/projects/' + encodeURIComponent(path) + '/repository/branches');
    url.searchParams.set('per_page', 100);
    url.searchParams.set('page', page);
    fetch(url)
    .then(response => {
      if (page < parseInt(response.headers.get('X-Total-Pages'), 10)) {
        fetchBranches(path, page + 1);
      }
      return response.json();
    })
    .then(branches => {
      for (let branch of branches) {
        const element = forks[path].issueFork.querySelector(".branches li[data-branch='" + Drupal.checkPlain(branch.name) + "']");
        if (element !== null) {
          // Fill out branch details.
          element.querySelector('.link').setAttribute('href', branch.web_url);
          const compareElement = element.querySelector('.compare');
          if (!compareElement.hasAttribute('href')) {
            (async () => {
              compareElement.setAttribute('href', forks[path].url + '/-/compare/' + (await forks[path].issueBranch) + '...' + branch.name);
            })();
          }
        }
      }
    });
  }

  function fetchMergeRequestActivity(projectPath, iid, cursor = '') {
    fetch(graphql, {
      body: JSON.stringify({
        query: `query mrActivityFetch($path: ID!, $iid: String!, $cursor: String!) {project(fullPath: $path) {
          mergeRequests(iids: [$iid]) {
            nodes {
              title
              descriptionHtml
              webUrl
              state
              mergeStatusEnum
              mergeError
              draft
              targetBranch
              sourceBranch
              sourceProjectId
              sourceProject {
                fullPath
              }
              diffRefs {
                headSha
              }
              notes (after: $cursor) {
                nodes {
                  id
                  discussion {
                    id
                  }
                  author {
                    name
                    username
                  }
                  body
                  bodyHtml
                  createdAt
                  resolvable
                  resolved
                  system
                }
                pageInfo {
                  endCursor
                  hasNextPage
                }
              }
              headPipeline {
                detailedStatus {
                  detailsPath
                  favicon
                  text
                }
              }
            }
          }
        }}`,
        variables: {
          path: projectPath,
          iid: iid,
          cursor: cursor
        }
      })
    })
    .then(response => response.json())
    .then(d => {
      const mergeRequestData = d.data.project.mergeRequests.nodes[0];
      if (!mergeRequestData) {
        return;
      }

      if (mergeRequestData.notes.pageInfo.hasNextPage) {
        fetchMergeRequestActivity(projectPath, iid, mergeRequestData.notes.pageInfo.endCursor);
      }

      if (cursor === '') {
        // Get merge request status.
        var status;
        if (mergeRequestData.state === 'merged') {
          status = Drupal.t('merged');
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class = 'merged';
        }
        else if (mergeRequestData.state === 'closed') {
          status = Drupal.t('closed');
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class = 'closed';
        }
        else if (mergeRequestData.draft) {
          status = Drupal.t('Draft');
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class = 'wip';
        }
        else if (mergeRequestData.mergeError !== null || mergeRequestData.mergeStatusEnum === 'CANNOT_BE_MERGED') {
          status = Drupal.t('merge error');
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class = 'error';
        }
        else if (mergeRequestData.mergeStatusEnum === 'CAN_BE_MERGED') {
          status = Drupal.t('mergeable');
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class = 'mergeable';
        }
        if (status !== undefined) {
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].element.insertAdjacentHTML('beforeend', ' ' + status);
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].element.classList.add(forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class);
        }

        const mergeOption = document.querySelector('#edit-merge-branch option[value="' + mergeRequestData.sourceProjectId + '-' + iid + '"]');
        if (mergeOption !== null) {
          if (mergeRequestData.draft || mergeRequestData.state !== 'opened' || mergeRequestData.mergeStatusEnum === 'CANNOT_BE_MERGED') {
            const mergeSelect = mergeOption.parentElement;
            mergeSelect.removeChild(mergeOption);
            // Hide if no options remain.
            if (mergeSelect.childElementCount <= 1) {
              const mergeElements = document.getElementById('drupalorg-merge-mr');
              mergeElements.parentElement.removeChild(mergeElements);
            }
          }
          else {
            mergeOption.insertAdjacentText('beforeend', ' → ' + mergeRequestData.targetBranch);
          }
        }

        const initialComment = document.querySelector('#comment-' + Drupal.settings.drupalorgBranchData[mergeRequestData.sourceBranch]['!' + iid].initialCid + '>.content');
        if (initialComment !== null) {
          initialComment.insertAdjacentHTML('beforeend', '<strong>' + Drupal.checkPlain(mergeRequestData.title) + '</strong><div class="merge-request-description">' + mergeRequestData.descriptionHtml + '</div>');
          postProcessGitLabHtml(initialComment.querySelector('.merge-request-description'), projectPath);
        }

        // Show pipeline status and link next to the MR.
        if (mergeRequestData.headPipeline !== null) {
          const headPipeline = mergeRequestData.headPipeline.detailedStatus;
          const mrLink = document.querySelector('[href="' + mergeRequestData.webUrl + '"]');
          mrLink.insertAdjacentHTML('beforebegin', '<a class="pipeline-result" href="' + Drupal.settings.drupalorgGitlabUrl + headPipeline.detailsPath + '" title="' + Drupal.t('Open @status pipeline', {'@status': headPipeline.text}) + '"><img src="' + Drupal.settings.drupalorgGitlabCiIconsPath + headPipeline.favicon + '.png' + '" alt="' + Drupal.t('Status: @status', {'@status': headPipeline.text}) + '"></a>');
        }
      }

      // Show/hide MR-comments list.
      let toggleLink = document.querySelector('.mr-activity-toggle');
      if (toggleLink === null) {
        const commentsHeading = document.querySelector('.comment-wrapper .title');
        toggleLink = document.createElement('a');
        toggleLink.className = "mr-activity-toggle";
        toggleLink.setAttribute('href', '#');
        toggleLink.addEventListener('click', e => {
          e.preventDefault();
          toggleMrComments();
          return false;
        });
        toggleLink.textContent = Drupal.t('Toggle MR activity');
        commentsHeading.insertAdjacentElement('afterend', toggleLink);
      }

      // Populate comment list.
      const comments = getComments();
      const commentTimes = Array.from(comments.keys()).sort().reverse();
      for (let note of mergeRequestData.notes.nodes) {
        if ((note.system && (note.body === 'closed' || note.body === 'reopened')) || note.author.username === 'tugboat') {
          continue;
        }
        // Find the most recent comment posted before the note, and set up
        // note(s) to be added after that comment.
        const createdAt = new Date(note.createdAt);
        const afterComment = comments.get(commentTimes.find(e => e < createdAt.getTime())) || comments.get(Math.min(...commentTimes));
        if (!afterComment.hasOwnProperty('mergeActivity')) {
          afterComment.noteElements = new Map();
          afterComment.mergeActivity = document.createElement('ul');
          afterComment.mergeActivity.classList.add('merge-request-activity');
          afterComment.comment.insertAdjacentElement('afterend', afterComment.mergeActivity);
        }
        const noteElement = document.createElement('li');
        const noteId = note.id.match(/\d+$/)[0];
        noteElement.setAttribute('id', 'mr' + iid + '-note' + noteId);
        var post = '';
        if (note.resolvable) {
          if (note.resolved) {
            post += ' <span class="resolved">' + Drupal.t('✓ resolved') + '</span>';
          }
          else {
            post += ' <span class="unresolved">' + Drupal.t('✗ unresolved') + '</span>';
          }
        }
        if (forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].discussions[note.discussion.id] === undefined) {
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].discussions[note.discussion.id] = [noteElement];
        }
        else {
          forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].discussions[note.discussion.id].push(noteElement);
        }
        noteElement.innerHTML = (Drupal.settings.drupalorgGitUsers[note.author.username] === undefined ? Drupal.settings.drupalorgGitUsers[''].picture : Drupal.settings.drupalorgGitUsers[note.author.username].picture) + Drupal.t('<div class="submitted"><a href="!url" class="merge-request !mergeClass">Merge request !!iid</a> !name updated <time datetime="@iso">@date at @time</time> <a href="#!id" class="permalink">#</a>!post</div>', {
          '!url': mergeRequestData.webUrl + '#note_' + noteId,
          '!mergeClass': forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].class,
          '!iid': iid,
          '!name': Drupal.settings.drupalorgGitUsers[note.author.username] === undefined ? Drupal.checkPlain(note.author.username) : '<a href="' + Drupal.settings.drupalorgGitUsers[note.author.username].url + '">' + Drupal.checkPlain(note.author.username) + '</a>',
          '@iso': createdAt.toISOString(),
          '@date': dateFormat.format(createdAt),
          '@time': timeFormat.format(createdAt),
          '!id': noteElement.getAttribute('id'),
          '!post': post
        }) + '<div class="merge-request-note">' + note.bodyHtml + '</div>';
        postProcessGitLabHtml(noteElement.querySelector('.merge-request-note'), projectPath, note);

        // Insert the note.
        if (afterComment.noteElements.has(createdAt.getTime())) {
          afterComment.noteElements.get(createdAt.getTime()).push(noteElement);
        }
        else {
          afterComment.noteElements.set(createdAt.getTime(), [noteElement]);
        }
        const beforeNote = Array.from(afterComment.noteElements.keys()).sort().find(e => e > createdAt.getTime());
        if (beforeNote === undefined) {
          afterComment.mergeActivity.insertAdjacentElement(createdAt.getTime() < afterComment.noteElements.keys().next().value ? 'afterbegin' : 'beforeend', noteElement);
        }
        else {
          afterComment.noteElements.get(beforeNote)[0].insertAdjacentElement('beforebegin', noteElement);
        }
        forks[mergeRequestData.sourceProject.fullPath].notesAdded += 1;
      }

      if (toggleLink !== null && forks[mergeRequestData.sourceProject.fullPath].notesAdded > 0) {
        // Do the first toggle according to user's settings after the first render.
        toggleMrComments(false);
        toggleLink.textContent = Drupal.t('Toggle MR activity') + ' ' + Drupal.formatPlural(forks[mergeRequestData.sourceProject.fullPath].notesAdded, '(@count comment)', '(@count comments)');
      }

      if (!mergeRequestData.notes.pageInfo.hasNextPage) {
        for (const [id, elements] of Object.entries(forks[mergeRequestData.sourceProject.fullPath].mergeRequests[iid].discussions)) {
          if (elements.length > 1) {
            const firstNote = elements.pop();
            const firstNoteId = firstNote.getAttribute('id');
            const firstLink = ' <a class="reply-to" href="#' + firstNoteId + '"><abbr title="' + Drupal.t('Reply to') + '">↪</abbr> ' + Drupal.checkPlain([...firstNote.querySelector('.merge-request-note').textContent.replace(/\n.*$/, '')].slice(0, 100).join('')) + '</a>';
            firstNote.classList.add('in-discussion');
            firstNote.classList.add('discussion-' + firstNoteId);
            for (let element of elements) {
              element.querySelector('.submitted').insertAdjacentHTML('afterend', firstLink);
              element.classList.add('in-discussion');
              element.classList.add('discussion-' + firstNoteId);
            }
          }
        }
      }
    });
  }

  function waitForImport(path) {
    fetch(graphql, {
      body: JSON.stringify({
        query: `query projectImport($path: ID!) {project(fullPath: $path) {
          importStatus
          description
        }}`,
        variables: {'path': path}
      })
    })
    .then(response => response.json())
    .then(r => {
      if (r.data.project.importStatus === 'started' || !r.data.project.description.startsWith('For collaboration')) {
        window.setTimeout(waitForImport, 500, path);
      }
      else {
        fetchBranches(path);
        forks[path].issueFork.classList.remove('in-progress');
        forks[path].issueFork.removeChild(forks[path].issueFork.querySelector('.in-progress'));
      }
    });
  }

  /**
   * Fetches Tugboat preview URLs.
   *
   * @param int gitlabProjectId
   *   The fork’s GitLab project ID.
   * @param issueFork
   *   The DOM issueFork.
   */
  function fetchTugboatPreviews(gitlabProjectId, issueFork) {
    fetch(Drupal.settings.basePath + 'drupalorg-issue-fork-tugboat-previews/' + gitlabProjectId, {credentials: 'include'})
    .then(response => response.json())
    .then(previews => {
      for (let preview of previews) {
        const branchElement = issueFork.querySelector(`[data-branch="` + preview.branch + `"]`);
        if (branchElement !== null) {
          const state = preview.state === 'suspended' ? preview.suspended : preview.state;
          branchElement.insertAdjacentHTML('beforeend', Drupal.t('<div class="tugboat"><span><a href="!url" target="_blank"><span><img src="/files/ads/tugboat.svg" width="20" height="20"> View live preview</a> (@state) <span class="description">via <a href="https://www.tugboatqa.com/drupal-welcome" target="_blank">Tugboat</a></span></span> <a href="!log_url" target="_blank">View log</a> <a href="!rebuild_url">Rebuild</a></div>', {
            '!url': preview.url,
            '@state': state,
            '!rebuild_url': Drupal.settings.basePath + 'drupalorg-issue-fork-tugboat-rebuild-preview/' + preview.id + '?destination=' + encodeURI(window.location.pathname),
            '!log_url': Drupal.settings.basePath + 'drupalorg-issue-fork-tugboat-preview-log/' + preview.id
          }));
        }
      }
    })
    .catch(error => {
      const branches = issueFork.querySelector('ul.branches');
      if (branches !== null) {
        branches.insertAdjacentHTML('beforeend', Drupal.t('<div class="tugboat"><span>Error loading live previews.</span></div>'));
      };
    });
  }

  function getComments() {
    if (commentList === null) {
      let lastDate;
      commentList = new Map();
      for (let comment of Array.from(document.querySelectorAll('.comments>.comment')).reverse()) {
        const date = Date.parse(comment.querySelector('time[pubdate]').getAttribute('datetime'));
        if (date > lastDate && lastDate !== undefined) {
          // Due to https://www.drupal.org/project/drupal/issues/1374090,
          // comments may be out of chronological order. Ignore time traveling.
          continue;
        }
        commentList.set(date, {
          comment: comment
        });
        lastDate = date;
      }
    }

    return commentList;
  }

  function postProcessGitLabHtml(element, projectPath, note = null) {
    // Add the GitLab domain for links & images.
    for (let link of element.querySelectorAll('a[href^="/"]:not([href^="//"])')) {
      link.setAttribute('href', Drupal.settings.drupalorgGitlabUrl + link.getAttribute('href'));
    }
    for (let image of element.querySelectorAll('img.lazy')) {
      const match = image.dataset.src.match(/^\/(?<projectPath>.+)\/uploads\/[0-9a-f]+\/[^\/]*$/);
      if (match !== null && match.groups.projectPath === projectPath) {
        image.setAttribute('loading', 'lazy');
        image.setAttribute('src', Drupal.settings.drupalorgGitlabUrl + match[0]);
      }
      else if (image.dataset.src.startsWith(Drupal.settings.drupalorgGitlabUrl)) {
        image.setAttribute('loading', 'lazy');
        image.setAttribute('src', image.dataset.src);
      }
    }

    // Add Show all/fewer commits toggle.
    if (note !== null && note.system) {
      const commitList = element.querySelector(':scope > ul');
      if (commitList !== null && commitList.childElementCount > 3) {
        element.classList.add('drupalorg-inline-collapsible');
        // Create see more button & place.
        const seeMore = document.createElement('button');
        seeMore.classList.add('see-more');
        element.insertAdjacentElement('beforeend', seeMore);
        seeMore.addEventListener('click', function (e) {
          if (commitList.classList.contains('collapsed')) {
            this.innerHTML= Drupal.t("Show fewer commits");
            commitList.classList.remove('collapsed');
          }
          else {
            this.innerHTML = Drupal.t("Show more commits");
            commitList.classList.add('collapsed');
          }
        });
        seeMore.click();
      }
    }
  }

})();

function toggleMrComments(setValue = true) {
  let currentValue = window.localStorage.getItem('drupalorg_merge_request_activity');
  const ancestor = document.querySelector('.merge-request-activity').closest('.comments');

  if (setValue) {
    currentValue = (currentValue == '0') ? '1' : '0';
    window.localStorage.setItem('drupalorg_merge_request_activity', currentValue);
  }

  if (currentValue == '0') {
    ancestor.classList.add('merge-request-activity-hide');
  }
  else {
    ancestor.classList.remove('merge-request-activity-hide');
  }

  return false;
}
