(function () {
  'use strict';

  Drupal.behaviors.drupalorgCopy = {
    attach: context => {
      // Replace jQuery with DOM object.
      if (context.jquery !== undefined) {
        context = context.get(0);
      }

      for (let input of context.querySelectorAll('.drupalorg-copy')) {
        if (input.parentElement.querySelector('button') !== null) {
          // Already attached.
          continue;
        }
        // Select on click.
        input.addEventListener('click', e => {
          e.target.select();
        });
        // Add copy button.
        const copyButton = document.createElement('button');
        copyButton.innerHTML = Drupal.settings.drupalOrg.copyIcon;
        copyButton.addEventListener('click', e => {
          e.preventDefault();
          input.select();
          document.execCommand('copy');
        });
        input.insertAdjacentElement('afterend', copyButton);
        input.parentElement.classList.add('drupalorg-copy-container');
      }
    }
  };
})();
