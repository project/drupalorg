# Tugboat test suite

This is a rudimentary test harness for the Tugboat integration.

It's meant to be executed locally in order to catch bugs quickly.

__CAUTION__ This suite overrides a few environment variables in order to
run locally.

## Requirements
Install Drupal locally:
```
drush si -y
drush en -y drupalorg
```

## Running the suite
Start the proxy in a separate terminal. This proxy mocks Tugboat API responses.

```
php -S localhost:8001 tugboatProxy.php
```

Run the test suite:

```
cd htdocs
drush php-script sites/all/modules/drupalorg/drupalorg/tests/tugboat/tugboat.php
```

The above command will throw a lot of warnings due to lack of configuration. Ignore it,
just look for PHP Errors and ensure that all checks start with an [OK] instead of a [KO].
