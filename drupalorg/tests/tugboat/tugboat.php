<?php

/**
 * @file
 *
 * Tugboat test suite.
 */

// Set variables.
variable_set('drupalorg_tugboat_api', 'http://localhost:8001');
variable_set('tugboat_project_id', '1234');
variable_set('versioncontrol_gitlab_url', 'http://localhost:8001');
variable_set('tugboat_gitlab_user_id', '72');

// TugboatApi tests.
// =================
// TugboatApi::getClient();
/** @var \TugboatApi $client */
$client = TugboatApi::getClient();
if (!empty($client)) {
  drush_print('[OK] TugboatApi::getClient() returns a valid client.');
}

// TugboatApi::rebuildPreview('foo');
TugboatApi::rebuildPreview('foo');
drush_print('[OK] TugboatApi::rebuildPreview() did not throw an Exception.');

// TugboatApi::getPreviewLog('foo');
$preview_log = TugboatApi::getPreviewLog('foo');
if (!empty($preview_log)) {
  drush_print('[OK] TugboatApi::getPreviewLog() returned a preview log.');
}
else {
  drush_print('[KO] TugboatApi::getPreviewLog() failed.');
}

// TugboatApi::getRepositoryId('project/drupal');
$repository = TugboatApi::getRepositoryId('project/drupal');
if (!empty($repository)) {
  drush_print('[OK] TugboatApi::getRepositoryId() returned a repository.');
}
else {
  drush_print('[KO] TugboatApi::getRepositoryId() failed.');
}

$response = TugboatApi::registerRepository('project', 'webform');
if ($response->getStatusCode() == 202) {
  drush_print('[OK] TugboatApi::registerRepository() returned a successful response.');
}
else {
  drush_print('[KO] TugboatApi::registerRepository() failed.');
}

$response = TugboatApi::updateRepositoryAuth('123repo');
if ($response->getStatusCode() == 200) {
  drush_print('[OK] TugboatApi::updateRepositoryAuth() returned a successful response.');
}
else {
  drush_print('[KO] TugboatApi::updateRepositoryAuth() failed.');
}


// TugboatApi::getProjectRepositories();
$repos = TugboatApi::getProjectRepositories();
if (!empty($repos[0])) {
  drush_print('[OK] TugboatApi::getProjectRepositories() returned repos.');
}
else {
  drush_print('[KO] TugboatApi::getProjectRepositories() failed.');
}

// TugboatApi::getPreviewsFor('project/drupal');
$previews = TugboatApi::getPreviewsFor('project/drupal');
if (!empty($previews[0])) {
  drush_print('[OK] TugboatApi::getPreviewsFor() returned previews.');
}
else {
  drush_print('[KO] TugboatApi::getPreviewsFor() failed.');
}

// TugboatPreview tests
// ====================
// TugboatPreview::fromGitLabPayload()

// Unsupported payload.
// Payload for core.
$unsupported_payload = new stdClass();
$unsupported_payload->project = new stdClass();
$unsupported_payload->project->path_with_namespace = 'project/drupal';
$unsupported_payload->project->name = 'drupal';
$unsupported_payload->project->namespace = 'project';
$unsupported_payload->object_attributes = new stdClass();
$unsupported_payload->object_attributes->iid = '72';
$unsupported_payload->object_attributes->source_branch = '1234_drupal';
$unsupported_payload->object_attributes->target_branch = '8.8.x';
$unsupported_payload->object_attributes->action = 'update';
try {
  $tugboat_preview = TugboatPreview::fromGitLabPayload($unsupported_payload);
  drush_print('[KO] TugboatPreview::fromGitLabPayload() when a merge request event is not supported, an exception should be thrown.');
}
catch (UnsupportedMergeRequestEventException $e) {
  drush_print('[OK] TugboatPreview::fromGitLabPayload() throws an exception when the merge request event is unsupported.');
}

// Payload for core.
$payload_for_core = new stdClass();
$payload_for_core->project = new stdClass();
$payload_for_core->project->path_with_namespace = 'project/drupal';
$payload_for_core->project->name = 'drupal';
$payload_for_core->project->namespace = 'project';
$payload_for_core->object_attributes = new stdClass();
$payload_for_core->object_attributes->iid = '72';
$payload_for_core->object_attributes->source_branch = '1234_drupal';
$payload_for_core->object_attributes->target_branch = '8.8.x';
$payload_for_core->object_attributes->action = 'open';

$tugboat_preview = TugboatPreview::fromGitLabPayload($payload_for_core);
if (!empty($tugboat_preview)) {
  drush_print('[OK] TugboatPreview::fromGitLabPayload() returned a payload.');
}
else {
  drush_print('[KO] TugboatPreview::fromGitLabPayload() failed.');
}

// Same, but for contrib.
$payload_for_contrib = new stdClass();
$payload_for_contrib->project = new stdClass();
$payload_for_contrib->project->path_with_namespace = 'project/webform';
$payload_for_contrib->project->name = 'webform';
$payload_for_contrib->project->namespace = 'project/webform';
$payload_for_contrib->object_attributes = new stdClass();
$payload_for_contrib->object_attributes->iid = '67';
$payload_for_contrib->object_attributes->source_branch = '1234_webform-test';
$payload_for_contrib->object_attributes->target_branch = '8.x-5.x';
$payload_for_contrib->object_attributes->action = 'open';
$payload_for_contrib->object_attributes->source = new stdClass();
$payload_for_contrib->object_attributes->source->path_with_namespace = 'project/webform';
$payload_for_contrib->object_attributes->target_project_id = '400';
$tugboat_preview = TugboatPreview::fromGitLabPayload($payload_for_contrib);
if (!empty($tugboat_preview)) {
  drush_print('[OK] TugboatPreview::fromGitLabPayload() returned a payload for contrib.');
}
else {
  drush_print('[KO] TugboatPreview::fromGitLabPayload() failed for contrib.');
}

// Now test a contrib that does not support tugboat.
$payload_for_unsupported_contrib = new stdClass();
$payload_for_unsupported_contrib->project = new stdClass();
$payload_for_unsupported_contrib->project->path_with_namespace = 'project/foo';
$payload_for_unsupported_contrib->project->name = 'webform';
$payload_for_unsupported_contrib->project->namespace = 'project/foo';
$payload_for_unsupported_contrib->object_attributes = new stdClass();
$payload_for_unsupported_contrib->object_attributes->iid = '67';
$payload_for_unsupported_contrib->object_attributes->source_branch = '1234_foo-test';
$payload_for_unsupported_contrib->object_attributes->target_branch = '8.x-5.x';
$payload_for_unsupported_contrib->object_attributes->action = 'open';
$payload_for_unsupported_contrib->object_attributes->source = new stdClass();
$payload_for_unsupported_contrib->object_attributes->source->path_with_namespace = 'project/foo';
$payload_for_unsupported_contrib->object_attributes->target_project_id = '400';
try {
  $tugboat_preview = TugboatPreview::fromGitLabPayload($payload_for_unsupported_contrib);
  drush_print('[KO] TugboatPreview::fromGitLabPayload() when a project does not support Tugboat, an exception should be thrown.');
}
catch (ProjectDoesNotSupportTugboatException $e) {
  drush_print('[OK] TugboatPreview::fromGitLabPayload() throws an exception when the project does not support Tugboat.');
}

// $tugboat_preview->build()
/** @var \Psr\Http\Message\ResponseInterface $response */
$response = $tugboat_preview->build();
if ($response->getStatusCode() == 202) {
  drush_print('[OK] $tugboat_preview->build() returned a successful response.');
}
else {
  drush_print('[KO] $tugboat_preview->build() failed.');
}

// TugboatPreview::fetchConfiguration()
$configuration = TugboatPreview::fetchConfiguration('drupal', 'default');
if (!empty($configuration)) {
  drush_print('[OK] TugboatPreview::fetchConfiguration() returned valid configuration.');
}
else {
  drush_print('[KO] TugboatPreview::fetchConfiguration() failed.');
}

// drush drupalorg-tugboat-register-project
// ========================================
drush_print('Testing drush drupalorg-tugboat-register-project');
drush_drupalorg_tugboat_register_project('project', 'drupal');

// drush drupalorg-tugboat-build-base-preview
// ==========================================
drush_print('Testing drupalorg-tugboat-build-base-preview');
drush_drupalorg_tugboat_build_base_preview('123projectid', 'drupal', '9.5.x');
