<?php

/**
 * @file
 *
 * Test proxy to mock requests to the Tugboat API.
 */

if ($_SERVER['REQUEST_URI'] == '/v3/previews/foo/rebuild') {
  http_response_code(202);
  echo 'ok';
}

if ($_SERVER['REQUEST_URI'] == '/v3/previews/foo/log') {
  http_response_code(200);
  echo json_encode(['foo' => 'bar']);
}

if ($_SERVER['REQUEST_URI'] == '/v3/projects/1234/repos') {
  http_response_code(200);
  echo json_encode([
    [
      'name' => 'project/drupal',
      'id' => '123repo',
      'provider_config' => [
        'server' => 'http://localhost:8001',
        'namespace' => 'project',
        'project' => 'drupal',
      ],
    ],
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/v3/repos/123repo/auth') {
  http_response_code(200);
  echo json_encode([
    'name' => 'project/drupal',
    'id' => '123repo',
    'provider_config' => [
      'server' => 'http://localhost:8001',
      'namespace' => 'project',
      'project' => 'drupal',
    ],
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/v3/repos/123repo/previews') {
  http_response_code(200);
  echo json_encode([
    [
      'id' => '123preview',
    ],
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/v3/previews') {
  $payload = json_decode(file_get_contents('php://input'));
  http_response_code(202);
  echo json_encode([
    'project'=> '5d810c19f6f8203d5b65ef01',
    'repo'=> '5d810c19f6f82083ed65ef03',
    'preview'=> '5d9b842252163ca8a1508c11',
    'action'=> 'build',
    'target'=> 'previews',
    'args'=> [],
    'createdAt'=> '2019-08-24T14:15:22Z',
    'endedAt'=> '2019-08-24T14:15:22Z',
    'id'=> '5d9e0a3f7f02ad896974a975',
    'job'=> '5d9e0a3f7f02ad896974a975',
    'key'=> '5d9b5bfb52163ce4e1508c07',
    'message'=> 'string',
    'object'=> '5d55823f30af7a1be3899ca4',
    'result'=> 'success',
    'startedAt'=> '2019-08-24T14:15:22Z',
    'type'=> 'job',
    'updatedAt'=> '2019-08-24T14:15:22Z'
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/v3/repos') {
  $payload = json_decode(file_get_contents('php://input'));
  http_response_code(202);
  echo json_encode([
    'foo' => 'bar',
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/api/v4/projects/400/members') {
  http_response_code(200);
  header('Content-Type: application/json');
  echo json_encode([
    'foo' => 'bar',
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/api/v4/projects/400/members/72') {
  http_response_code(200);
  header('Content-Type: application/json');
  echo json_encode([
    'foo' => 'bar',
  ]);
}

if ($_SERVER['REQUEST_URI'] == '/project/webform/-/raw/1234_webform-test/.tugboat/config.yml') {
  echo '- drush en webform -y';
}

