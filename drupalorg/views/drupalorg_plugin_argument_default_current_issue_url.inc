<?php

/**
 * Provide a default argument of the current issue URL.
 */
class drupalorg_plugin_argument_default_current_issue_url extends views_plugin_argument_default {

  /**
   * {@inheritdoc}
   */
  public function get_argument() {
    if (arg(0) === 'node' && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      if ($node->type == 'project_issue') {
        return url('node/' . $node->nid, ['absolute' => TRUE, 'alias' => TRUE]);
      }
    }

    return '';
  }

}
