<?php

/**
 * Views field handler for determining if a release has security advisory
 * coverage.
 */
class drupalorg_handler_release_branch_info extends views_handler_field_entity {
  function render($values) {
    static $releases_seen = [];
    static $has_api_tid_regex;

    if ($release_wrapper = entity_metadata_wrapper('node', $this->get_value($values))) {
      $output = '';
      // Install with Composer information.
      if ($release_wrapper->field_release_build_type->value() === 'static' && $release_wrapper->field_release_category->value() === 'current' && ($composer_command = drupalorg_composer_command_display($release_wrapper))) {
        $output .= '<div class="install">' . t('Install:') . ' ' . $composer_command . '</div>';
      }

      // Find a pre-release, if there is one.
      if (($nid = db_query('SELECT latest_release FROM {project_release_supported_versions} WHERE recommended_release = :nid AND latest_release != :nid', [':nid' => $release_wrapper->getIdentifier()])->fetchField()) && ($pre_release = node_load($nid))) {
        $pre_release_wrapper = entity_metadata_wrapper('node', $pre_release);
        $output .= '<p>' . t('Pre-release version: !version <small>released @updated</small>', [
          '!version' => l($pre_release_wrapper->field_release_version->raw(), 'node/' . $pre_release->nid),
          '@updated' => format_date($pre_release->changed),
        ]);
        if ($short_description = $pre_release_wrapper->field_release_short_description->value(['sanitize' => TRUE])) {
          $output .= '<br>' . $short_description;
        }
        $output .= '</p>';
      }

      // Find the dev release with the same project and version number components.
      $branch = project_release_get_branch($release_wrapper->field_release_version->value());
      $result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
        ->propertyCondition('type', project_release_release_node_types())
        ->fieldCondition('field_release_project', 'target_id', $release_wrapper->field_release_project->raw())
        ->fieldCondition('field_release_build_type', 'value', 'dynamic')
        ->fieldCondition('field_release_version', 'value', db_like($branch) . '%', 'LIKE')
        ->fieldCondition('field_release_version', 'value', db_like($branch) . '%.%', 'NOT LIKE')
        ->execute();
      if (!isset($result['node'])) {
        if (is_null($has_api_tid_regex)) {
          $has_api_tid_regex = project_release_has_api_tid_regex();
        }
        if (!preg_match($has_api_tid_regex, $branch)) {
          // Also consider {major}.x dev release, in addition to {major}.{minor}.x.
          preg_match('/^(?<major_branch>[0-9]+\.)/', $branch, $match);
          $result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
            ->propertyCondition('type', project_release_release_node_types())
            ->fieldCondition('field_release_project', 'target_id', $release_wrapper->field_release_project->raw())
            ->fieldCondition('field_release_build_type', 'value', 'dynamic')
            ->fieldCondition('field_release_version', 'value', $match['major_branch'] . 'x-dev')
            ->execute();
        }
      }
      if (isset($result['node'])) {
        $dev_release = node_load(array_keys($result['node'])[0]);
        $dev_release_wrapper = entity_metadata_wrapper('node', $dev_release);

        // Only show each dev release once. May be with a tagged release, or
        // fall back to the development display. This class is instanciated for
        // each display, so need the extra check for the current display.
        if (($this->view->current_display !== 'development' || empty($releases_seen[$dev_release->nid])) && $dev_release_wrapper->field_show_download->raw()) {
          if ($this->view->current_display !== 'development') {
            $releases_seen[$dev_release->nid] = TRUE;
          }
          $output .= '<p>' . t('Development version: !version <small>updated @updated</small>', [
            '!version' => l($dev_release_wrapper->field_release_version->raw(), 'node/' . $dev_release->nid),
            '@updated' => format_date($dev_release->changed, 'medium', '', 'UTC'),
          ]) . '</p>';

          // GitlabCI testing.
          if ($repo = versioncontrol_project_repository_load($release_wrapper->field_release_project->raw())) {
            $output .= '<span class="pipeline-result" data-repo="' . url($repo->getUrlHandler()->getRepositoryViewUrl(), ['external' => TRUE]) . '" data-version="' . check_plain($dev_release_wrapper->field_release_vcs_label->raw()) . '"></span>';
          }
        }
      }

      // This output is heavily cached (not even "cc all"). A workaround is to
      // go to the "Administer releases" page of the project and Save.
      return $output;
    }
  }
}
