<?php

/**
 * Views field handler for showing a release’s core version requirement.
 */
class drupalorg_handler_release_core_version_requirement extends views_handler_field_entity {

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return drupalorg_release_core_version_requirement(entity_metadata_wrapper('node', $entity));
    }
  }

}
