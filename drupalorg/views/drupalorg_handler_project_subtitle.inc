<?php

/**
 * View field handler adding additional context, such as “in Drupal Core”.
 */
class drupalorg_handler_project_subtitle extends views_handler_field_entity {
  function render($values) {
    if (in_array($this->get_value($values)->nid, variable_get('drupalorg_projects_in_core', []))) {
      return t('in Drupal Core');
    }
  }
}
