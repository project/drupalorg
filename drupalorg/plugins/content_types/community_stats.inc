<?php

$plugin = [
  'title' => t('Community stats'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_community_stats_pane',
];

/**
 * Add information about becoming a guide maintainer.
 */
function drupalorg_community_stats_pane() {
  $activity = drupalorg_get_activity();
  $block = new stdClass();
  $block->content = t('<div class="column-content-region">
    <div class="users"><strong>@people_credited_year people credited</strong><br>in the last year</div>
  </div>
  <div class="column-content-region">
    <div class="commits"><strong>@issue_credits_week issue credits</strong><br>given in the last week</div>
  </div>
  <div class="column-content-region">
    <div class="comments"><strong>@number_of_issue_comments comments</strong><br>in the last week</div>
  </div>', [
    '@people_credited_year' => $activity['people_credited_year'],
    '@issue_credits_week' => $activity['issue_credits_week'],
    '@number_of_issue_comments' => $activity['number_of_issue_comments'],
  ]);

  return $block;
}
