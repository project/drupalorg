<?php

$plugin = [
  'title' => t('Section contents from OG menu'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_section_contents',
  'required context' => new ctools_context_required(t('Node'), 'node'),
];

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function drupalorg_section_contents($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();
  $block->content = '';
  if (isset($context->keyword) && $context->keyword === 'node' && isset($context->data)) {
    // Find nodes in the menu.
    if (($menus = og_menu_get_group_menus(['node' => [$context->data->nid]])) && ($menu = menu_tree($menus[0]['menu_name']))) {
      $nids = [];
      foreach (element_children($menu) as $key) {
        if ($menu[$key]['#original_link']['router_path'] === 'node/%') {
          $nids[] = preg_replace('#^node/#', '', $menu[$key]['#original_link']['link_path']);
        }
      }

      // Render those nodes.
      if (!empty($nids)) {
        $menu_names = array_column(og_menu_get_group_menus(['node' => $nids]), 'menu_name', 'gid');
        foreach (node_load_multiple($nids) as $node) {
          $wrapper = entity_metadata_wrapper('node', $node);
          $uri = entity_uri('node', $node);
          $block->content .= '<section><h2>' . l($node->title, $uri['path'], $uri['options']) . '</h2><p>' . check_plain($wrapper->field_summary->value()) . '</p>';
          if (isset($menu_names[$node->nid]) && ($menu = menu_tree($menu_names[$node->nid]))) {
            // Reduce classes added.
            unset($menu['#theme_wrappers']);
            foreach (element_children($menu) as $key) {
              unset($menu[$key]['#attributes']['class']);
            }
            $block->content .= '<ul class="guide-contents">';
            $block->content .= drupal_render($menu);
            $block->content .= '</ul>';
          }

          $block->content .= '</section>';
        }
      }
    }
  }

  return $block;
}
