<?php

$plugin = [
  'title' => t('Drupal Steward price calculator'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_steward_pricing',
];

/**
 * Add information about becoming a guide maintainer.
 */
function drupalorg_steward_pricing() {
  $block = new stdClass();
  $block->content = drupal_get_form('drupalorg_steward_pricing_form');
  return $block;
}

function drupalorg_steward_pricing_form() {
  $rates = [
    'requests_included' => 100000,
    0 => [
      'monthly' => 1500,
      'overage' => 10,
    ],
    1 => [
      'monthly' => 1000,
      'overage' => 7.5,
    ],
  ];

  $form = [
    '#attached' => [
      'js' => [
        drupal_get_path('module', 'drupalorg') . '/js/steward.js',
        ['type' => 'setting', 'data' => ['drupalorgSteward' => $rates]],
      ],
    ],
    'domains' => [
      '#title' => t('Number of domains'),
      '#type' => 'textfield',
      '#size' => 10,
      '#value' => 1,
    ],
    'requests' => [
      '#title' => t('Monthly requests'),
      '#type' => 'textfield',
      '#size' => 10,
      '#value' => $rates['requests_included'],
      '#description' => t('@requests are included each month for each domain', [
        '@requests' => number_format($rates['requests_included']),
      ]),
    ],
    'plan' => [
      '#title' => t('Customer of Drupal Association <a href="!url">Supporting Partner</a>?', [
        '!url' => url('drupal-services', ['query' => ['field_organization_support_value[1]' => 1]]),
      ]),
      '#type' => 'checkbox',
    ],
    'rates' => [],
    'estimate' => [
      '#markup' => '<div class="estimate">' . t('Estimate <strong>$calculating…</strong> USD / month') . '</div>',
    ],
  ];

  foreach ([0, 1] as $plan) {
    $form['rates'][$plan] = [
      '#prefix' => '<div class="plan element-hidden plan-' . $plan . '">',
      '#markup' => t('$@monthly per domain, $@overage per 10,000 requests <small>over the included requests</small>', [
        '@monthly' => number_format($rates[$plan]['monthly'] / 100, 2),
        '@overage' => number_format($rates[$plan]['overage'] / 100, 3),
      ]),
      '#suffix' => '</div>',
    ];
  }

  return $form;
}
