<?php

$plugin = [
  'title' => t('Security risk calculator'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_security_risk_calculator',
];

/**
 * Security risk calculator.
 */
function drupalorg_security_risk_calculator() {
  $block = new stdClass();
  $block->content = drupal_get_form('drupalorg_riskcal_form');
  return $block;
}
