<?php

$plugin = [
  'title' => t('Drupal Association donors'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'defaults' => [
    'since' => '2004-11-10',
    'limit' => 50,
    'show-subscription-type' => 'donation',
    'show-donor-type' => 'all',
    'campaign' => [],
  ],
];

function drupalorg_donors_content_type_edit_form($form, &$form_state) {
  $form['show-subscription-type'] = [
    '#type' => 'radios',
    '#title' => t('Show'),
    '#options' => [
      'donation' => t('Donations'),
      'membership' => t('Memberships'),
    ],
  ];
  $form['show-donor-type'] = [
    '#type' => 'radios',
    '#title' => t('Show'),
    '#options' => [
      'all' => t('Individuals & organizations'),
      'individual' => t('Individuals'),
      'organization' => t('Organizations'),
    ],
  ];
  $form['campaign'] = [
    '#type' => 'checkboxes',
    '#title' => t('Campaign'),
    '#options' => [
      'drupalcares_2020' => t('DrupalCares 2020'),
    ],
  ];
  $form['since'] = [
    '#type' => 'textfield',
    '#title' => t('Show donations since'),
    '#description' => t('Either <em>YYYY-MM-DD</em> or <em>-1 year</em>, see <a href="https://www.php.net/manual/en/datetime.formats.date.php">PHP date formats</a>.'),
  ];
  $form['limit'] = [
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#field_prefix' => t('Show the most recent'),
    '#field_suffix' => t('donors'),
  ];

  // Copy in default values.
  foreach ($form_state['plugin']['defaults'] as $key => $value) {
    $form[$key]['#default_value'] = $form_state['conf'][$key] ?: $value;
  }

  return $form;
}

function drupalorg_donors_content_type_edit_form_submit($form, &$form_state) {
  // Copy everything with a default to conf.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Add information about recent donors.
 */
function drupalorg_donors_content_type_render($subtype, $conf) {
  $block = new stdClass();
  $block->content = [
    'names' => [
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
    ],
  ];

  // Set up basic query, filtering by subscription type & date.
  $query = db_select('drupalorg_chargify_subscriptions', 'dcs')
    ->condition('dcs.last_payment', strtotime($conf['since']), '>=')
    ->fields('dcs', ['currency']);
  switch ($conf['show-subscription-type']) {
    case 'donation':
      $query->condition('dcs.product_handle', 'donation');
      if ($conf['show-donor-type'] === 'organization') {
        $query->condition('dcs.on_behalf', '_%', 'LIKE')
          ->condition('dcs.customer_organization', '_%', 'LIKE');
      }
      elseif ($conf['show-donor-type'] === 'individual') {
        $query->condition(db_or()->isNull('dcs.on_behalf')->condition('dcs.on_behalf', '_%', 'NOT LIKE')
          ->condition('dcs.customer_organization', '_%', 'NOT LIKE'));
      }
      break;

    case 'membership':
      if ($conf['show-donor-type'] === 'all') {
        $query->condition(db_or()
          ->condition('dcs.product_handle', 'individual%', 'LIKE')
          ->condition('dcs.product_handle', 'organization%', 'LIKE')
        );
      }
      else {
        $query->condition('dcs.product_handle', db_like($conf['show-donor-type']) . '%', 'LIKE');
      }
      break;
  }
  if (!empty($conf['campaign']['drupalcares_2020'])) {
    $query->condition('dcs.campaign_drupalcares_2020', 1);
  }

  // Get totals, and add sort.
  $totals_query = clone $query;
  switch ($conf['show-subscription-type']) {
    case 'donation':
      $totals_query->addExpression('sum(dcs.current_billing_amount_in_cents)', 'current_billing_amount_in_cents');
      $totals_query->addExpression('count(1)', 'donors');
      $result = $totals_query->groupBy('currency')
        ->orderBy('sum(current_billing_amount_in_cents)', 'DESC')
        ->execute();
      $totals = [];
      $donors = 0;
      foreach ($result as $row) {
        $totals[] = _format_chargify_money($row);
        $donors += $row->donors;
      }
      $block->content['#prefix'] = '<h3>' . t('We’ve raised @amount from @donors donors', [
        '@amount' => implode(t(' and '), $totals),
        '@donors' => $donors,
      ]) . '</h3>';
      break;

    case 'membership':
      $totals_query->addExpression('count(1)', 'members');
      $donors = $totals_query->execute()->fetch()->members;
      $block->content['#prefix'] = '<h3>' . format_plural($donors, '1 new or renewing member', '@count new & renewing members') . '</h3>';
      break;
  }

  // Complete query for specific people/organizations, filtering out anyone
  // opting out of listings.
  $result = $query->condition(db_or()->isNull('dcs.acknowledgement')->condition('dcs.acknowledgement', 'no%', 'NOT LIKE'))
    ->fields('dcs', [
      'uid',
      'product_handle',
      'organization_nid',
      'current_billing_amount_in_cents',
      'customer_first_name',
      'customer_last_name',
      'customer_organization',
      'on_behalf',
    ])
    ->orderBy('dcs.last_payment', 'DESC')
    ->range(0, $conf['limit'])
    ->execute()->fetchAll(PDO::FETCH_ASSOC);
  $users = user_load_multiple(array_column($result, 'uid'));
  $organizations = [];
  switch ($conf['show-subscription-type']) {
    case 'donation':
      if ($organization_titles = array_keys(array_filter(array_combine(array_column($result, 'customer_organization'), array_column($result, 'on_behalf'))))) {
        $organization_result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'organization')
          ->propertyCondition('status', NODE_PUBLISHED)
          ->propertyCondition('title', $organization_titles, 'IN')
          ->execute();
      }
      if (!empty($organization_result['node'])) {
        foreach (node_load_multiple(array_keys($organization_result['node'])) as $node) {
          $organizations[$node->title] = entity_metadata_wrapper('node', $node);
        }
      }
      break;

    case 'membership':
      if ($organization_nids = array_filter(array_column($result, 'organization_nid'))) {
        $organizations = node_load_multiple($organization_nids);
      }
      break;
  }
  $default_organization = theme('image', [
    'path' => drupal_get_path('module', 'drupalorg') . '/images/icon-organization.svg',
    'alt' => t('Organization icon'),
    'width' => 40,
    'height' => 40,
    'attributes' => [
      'class' => ['picture'],
      'loading' => 'lazy',
    ],
  ]);
  foreach ($result as $row) {
    if (($conf['show-subscription-type'] === 'donation' && !empty($row['on_behalf']) && !empty($row['customer_organization'])) || ($conf['show-subscription-type'] === 'membership' && strpos($row['product_handle'], 'organization') === 0)) {
      $organization_wrapper = NULL;
      switch ($conf['show-subscription-type']) {
        case 'donation':
          if (isset($organizations[$row['customer_organization']])) {
            $organization_wrapper = $organizations[$row['customer_organization']];
          }
          break;

        case 'membership':
          if (isset($organizations[$row['organization_nid']])) {
            $organization_wrapper = entity_metadata_wrapper('node', $organizations[$row['organization_nid']]);
          }
          break;
      }
      if (!is_null($organization_wrapper)) {
        if ($logo = $organization_wrapper->field_logo->value()) {
          $image = theme('image', [
            'path' => drupalorg_ensure_image_style('thumbnail', $logo['uri']),
            'alt' => t('@name’s logo', ['@name' => $organization_wrapper->label()]),
            'width' => 40,
            'height' => 40,
            'attributes' => [
              'class' => ['picture'],
              'loading' => 'lazy',
            ],
          ]);
        }
        else {
          $image = $default_organization;
        }
        $name = l($organization_wrapper->label(), 'node/' . $organization_wrapper->getIdentifier());
      }
      else {
        $image = $default_organization;
        $name = check_plain($row['customer_organization']);
      }
    }
    else {
      if (!isset($users[$row['uid']])) {
        continue;
      }
      if ($row['uid'] == 0) {
        if (!empty($row['customer_first_name']) || !empty($row['customer_last_name'])) {
          $name_display = check_plain($row['customer_first_name'] . ' ' . $row['customer_last_name']);
        }
        else {
          $name_display = t('Anonymous');
        }
        $name = $name_display;
      }
      else {
        $user_wrapper = entity_metadata_wrapper('user', $users[$row['uid']]);
        $first_name = $user_wrapper->field_first_name->value();
        $last_name = $user_wrapper->field_last_name->value();
        if (!empty($first_name) && !empty($last_name) && $first_name . ' ' . $last_name === $users[$row['uid']]->name) {
          $name_display = check_plain($users[$row['uid']]->name);
        }
        elseif (!empty($first_name) || !empty($last_name)) {
          $name_display = t('@first_name @last_name (@user_name)', ['@first_name' => $first_name, '@last_name' => $last_name, '@user_name' => $users[$row['uid']]->name]);
        }
        else {
          $name_display = check_plain($users[$row['uid']]->name);
        }
        $name = l($name_display, 'user/' . $row['uid'], ['html' => TRUE]);
      }
      if (!empty($users[$row['uid']]->picture)) {
        if (is_numeric($users[$row['uid']]->picture)) {
          $filepath = file_load($users[$row['uid']]->picture)->uri;
        }
        else {
          $filepath = $users[$row['uid']]->picture->uri;
        }
      }
      else {
        $filepath = variable_get('user_picture_default');
      }
      $image = theme('image', [
        'path' => drupalorg_ensure_image_style('drupalorg_user_picture', $filepath),
        'alt' => t('!user’s picture', ['!user' => $name_display]),
        'width' => 40,
        'height' => 40,
        'attributes' => [
          'class' => ['picture'],
          'loading' => 'lazy',
        ],
      ]);
    }
    switch ($conf['show-subscription-type']) {
      case 'donation':
        $text = t('!amount from !name', [
          '!amount' => _format_chargify_money((object) $row),
          '!name' => $name,
        ]);
        break;

      case 'membership':
        $text = $name;
        break;
    }
    $block->content['names'][] = [
      '#prefix' => '<li>',
      '#markup' => $image . ' ' . $text,
      '#suffix' => '</li>',
    ];
  }
  $displayed_count = count(element_children($block->content['names']));
  if ($displayed_count < $donors) {
    $block->content['names'][] = [
      '#markup' => '<em>' . format_plural($donors - $displayed_count, 'and one more', 'and @count more') . '</em>',
    ];
  }

  return $block;
}

/**
 * Format the billing amount of a Chargify subscription.
 */
function _format_chargify_money($row) {
  static $symbols = [
    'usd' => '$',
    'eur' => '€',
  ];

  return $symbols[$row->currency] . number_format(floor($row->current_billing_amount_in_cents / 100)) . '.' . str_pad($row->current_billing_amount_in_cents % 100, 2, '0', STR_PAD_LEFT);
}
