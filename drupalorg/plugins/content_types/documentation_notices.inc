<?php

$plugin = [
  'title' => t('Documentation notices'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_documentation_notices_menu',
  'required context' => new ctools_context_required(t('Node'), 'node'),
];

/**
 * Add information that needs to be seen under every piece of content in a guide.
 */
function drupalorg_documentation_notices_menu($subtype, $conf, $args, $context = NULL) {
  $show_d7_eol = FALSE;
  foreach (drupalorg_get_parents($context->data) as $parent) {
    if ($parent->nid == 2743405) {
      $show_d7_eol = TRUE;
      break;
    }
  }

  $block = new stdClass();
  if ($show_d7_eol || $context->argument == 2743405) {
    $block->content = '<p class="deprecated">' . t('Drupal 7 will no longer be supported after January 5, 2025. <a href="@migration-partners">Learn more and find resources for Drupal 7 sites</a>', [
      '@migration-partners' => url('about/drupal-7/d7eol/partners'),
    ]) . '</p>';
  }
  return $block;
}
