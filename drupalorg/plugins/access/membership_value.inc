<?php

/**
 * @file
 * Plugin to provide access control/visibility based on specified context string matching user-specified string
 */

$plugin = [
  'title' => t('Drupal Association Individual Membership'),
  'description' => t('Control access by Membership.'),
  'callback' => 'drupalorg_membership_value_access_check',
  'settings form' => 'drupalorg_membership_value_access_settings',
  'summary' => 'drupalorg_membership_value_access_summary',
  'required context' => new ctools_context_required(t('User'), 'user'),
  'defaults' => ['value' => []],
];

/**
 * Settings form.
 */
function drupalorg_membership_value_access_settings($form, &$form_state, $conf) {
  $form['settings']['value'] = [
    '#type' => 'checkboxes',
    '#title' => t('Membership status'),
    '#options' => [
      'New' => t('New'),
      'Current' => t('Current'),
      'Grace Period' => t('Grace Period'),
    ],
    '#default_value' => isset($conf['value']) ? $conf['value'] : [],
  ];

  return $form;
}

/**
 * Check for access.
 */
function drupalorg_membership_value_access_check($conf, $context) {
  // Check to see if the fields exist:
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  if (!isset($context->data->field_da_ind_membership)) {
    return FALSE;
  }

  if (isset($context->data->field_da_ind_membership[LANGUAGE_NONE][0]['value'])) {
    $string = $context->data->field_da_ind_membership[LANGUAGE_NONE][0]['value'];
  }
  else {
    $string = NULL;
  }

  return !empty($conf['value'][$string]);
}

/**
 * Provide a summary description based upon the specified context.
 */
function drupalorg_membership_value_access_summary($conf, $context) {
  return t('@identifier’s membership is @value', ['@identifier' => $context->identifier, '@value' => implode(', ', array_map('t', array_filter($conf['value'])))]);
}
