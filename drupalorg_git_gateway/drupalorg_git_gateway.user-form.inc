<?php

/**
 * @file
 * Forms for Drupal.org Git Gateway.
 */

function drupalorg_git_gateway_user_page($account) {
  global $user;

  // Pre-populate the form state with account information.
  $form_state = array(
    'account' => $account,
  );

  // Show one form if we're the user who owns this account
  if ($user->uid == $account->uid) {
    return drupal_build_form('drupalorg_git_gateway_user_form', $form_state);
  }
  // But show an entirely different form if we're another user (an admin).
  elseif (user_access('administer users') || user_access('administer user git access')) {
    return drupal_build_form('drupalorg_git_gateway_admin_user_form', $form_state);
  }
  else {
    // Or deny access.
    return drupal_access_denied();
  }
}

/**
 * Form callback, for GitLab access.
 */
function drupalorg_git_gateway_user_form($form, &$form_state) {
  $account = $form_state['account'];
  $form['#submit'][] = 'drupalorg_git_gateway_user_form_submit';

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 30,
  ];

  // Warn the user if their account has been suspended.
  if (!empty($account->git_disabled)) {
    drupal_set_message(t('Your git.drupalcode.org access has been suspended by a Drupal.org administrator. See <a href="!handbook">the handbook</a> for information about reinstating your account.', [
      '!handbook' => url('node/1042972'),
    ]), 'error');
  }

  // Both 'Git administrator' and 'User administrator' roles get admin powers here.
  if (user_access('administer users') || user_access('administer user git access')) {
    $form['git_administration'] = _drupalorg_git_gateway_user_admin($account);
    // Tack on the submit handler for the admin portion of the form.
    array_unshift($form['#submit'], 'drupalorg_git_gateway_admin_user_form_submit');
  }

  // If everything’s set in the expected state, show configuration helper text.
  if (!empty($account->git_consent) && !empty($account->git_username)) {
    $form['git_config'] = _drupalorg_git_gateway_user_config($account);
  }
  $form['username'] = _drupalorg_git_gateway_username_set($account, $form_state);

  return $form;
}

/**
 * Form callback, admin access.
 */
function drupalorg_git_gateway_admin_user_form($form, &$form_state) {
  $form = [];
  $account = $form_state['account'];
  $form['user_info'] = [
    '#type' => 'fieldset',
    '#title' => t("%username’s information", ['%username' => $account->name]),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => -20,
  ];
  $form['user_info']['username'] = [
    '#type' => 'item',
    '#title' => t('Current username'),
    '#markup' => $account->git_username ?: t('(Has not yet chosen a username)'),
    '#weight' => -20,
  ];

  $form['git_administration'] = _drupalorg_git_gateway_user_admin($account, FALSE);

  // Attach both the base and admin form submit handlers.
  $form['#submit'] = array(
    'drupalorg_git_gateway_admin_user_form_submit',
    'drupalorg_git_gateway_user_form_submit',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 30,
  );
  return $form;
}

function _drupalorg_git_gateway_username_set($account, &$form_state) {
  $form = [
    'username_container' => [],
  ];
  if (!empty($account->git_username)) {
    $form['access'] = [
      'username' => [
        '#type' => 'item',
        '#title' => t('Your git.drupalcode.org username'),
        '#markup' => $account->git_username,
      ],
      '#weight' => -1,
    ];
    $form['username_container'] = [
      '#type' => 'fieldset',
      '#title' => t('Change username'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'git_username' => [],
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Save'),
      ],
    ];
  }
  $form['username_container']['git_username'] = [
    '#type' => 'textfield',
    '#title' => t('Requested username'),
    '#required' => TRUE,
    '#element_validate' => ['drupalorg_git_gateway_username_validate'],
    '#maxlength' => 64,
    '#default_value' => $account->git_username ?: $account->name,
    '#description' => t('Can contain A-Z letters, numbers, periods, underscores, and dashes. Can not start with a dash; or end with a dash, dot, <code>.git</code>, or <code>.atom</code>.'),
  ];

  // Initial form load, proactively check for a valid username.
  if (empty($form_state['input'])) {
    drupalorg_git_gateway_username_validate($form['username_container']['git_username'], $form_state);
    if (empty($account->git_username) && empty(form_get_errors())) {
      // They have not set up a GitLab account yet, and their Drupal username
      // is a valid GitLab username. Go ahead and set up GitLab access.
      $form_state['values'] = [
        'git_username' => $form['username_container']['git_username']['#default_value'],
      ];
      drupalorg_git_gateway_user_form_submit($form, $form_state);
      drupal_goto(current_path());
    }
  }
  return $form;
}

function _drupalorg_git_gateway_user_config($account) {
  $user_config = t('Your commits may not be associated with your account unless you configure your Git email address to either <a href="!emails">an address associated with your account</a> or the anonymized email provided below. Read <a href="!handbook">the handbook</a> for a full discussion of identifying yourself to Git. We recommend:', [
    '!handbook' => url('node/1542048'),
    '!emails' => url(variable_get('versioncontrol_gitlab_url') . '/-/profile/emails', ['external' => TRUE]),
  ]);
  $user_config .= '<div class="codeblock"><code>';
  $user_config .= 'git config --global user.name ' . check_plain(escapeshellarg(drupalorg_git_gateway_author_name($account))) . '<br>';
  $user_config .= 'git config --global user.email ' . check_plain(escapeshellarg($account->mail)) . '<br>';
  $user_config .= '</code></div>';
  $user_config .= t('Email addresses in Git repositories can be accessed by anyone looking at the Git log. It is generally considered appropriate to use a real, public-facing email for Git. If you prefer to use an anonymized address, use:');
  $user_config .= '<div class="codeblock"><code>git config --global user.email ' . check_plain(escapeshellarg(drupalorg_git_gateway_anonymous_commit_email($account))) . '</code></div>';

  return [
    'user_config' => [
      '#type' => 'item',
      '#title' => t('Git configuration'),
      '#markup' => $user_config,
    ],
    '#weight' => 15,
  ];
}

function _drupalorg_git_gateway_user_admin($account) {
  $form = [
    '#type' => 'fieldset',
    '#title' => t('Account administration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 20,
  ];

  $form['git_disabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Disable git.drupalcode.org access for this user'),
    '#default_value' => $account->git_disabled,
  ];

  $form['git_disabled_msg'] = [
    '#type' => 'textarea',
    '#title' => t('Access disabled e-mail'),
    '#default_value' => t('@user_name,

Your access to git.drupalcode.org has been disabled because it is in violation of the Drupal Git Contributor Agreement & Repository Usage Policy at https://www.drupal.org/git-repository-usage-policy. To seek reinstatement, post an explanation of what happened, and the steps you will take to correct the situation, to the Drupal.org site moderators issue queue (https://www.drupal.org/docs/develop/git/setting-up-git-for-drupal/obtaining-git-access#s-reinstating-git-access).

@admin_name'),
    '#description' => t('Message to send to a user when disabling their access.'),
    '#rows' => 10,
  ];

  // Since we don't send an e-mail when enabling hide for disabled users.
  if ($account->git_disabled) {
    $form['git_disabled_msg']['#access'] = FALSE;
  }

  $form['git_vetted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is a vetted user'),
    '#description' => t('Vetted users can opt their projects into security advisory coverage.'),
    '#default_value' => $account->git_vetted,
  );

  return $form;
}

/**
 * Element validate callback.
 */
function drupalorg_git_gateway_username_validate($element, $form_state) {
  $username_check = drupalorg_git_gateway_username_check(isset($element['#value']) ? $element['#value'] : $element['#default_value'], $form_state['account']->uid);
  if ($username_check == DRUPALORG_GIT_USERNAME_TAKEN) {
    form_set_error('git_username', t('The requested username is already taken.'));
  }
  elseif ($username_check == DRUPALORG_GIT_USERNAME_INVALID_PATTERN) {
    form_set_error('git_username', t('The requested username contains invalid characters.'));
  }
  elseif ($username_check == DRUPALORG_GIT_USERNAME_INVALID_SPECIAL_CHARACTERS) {
    form_set_error('git_username', t('The requested username must not start or end with a special character and must not contain consecutive special characters.'));
  }
  elseif ($username_check == DRUPALORG_GIT_USERNAME_INVALID_CONTAINS_RESERVED_NAME) {
    form_set_error('git_username', t('The requested username is reserved, please choose a different username.'));
  }
  elseif ($username_check == DRUPALORG_GIT_USERNAME_INVALID_CONTAINS_FILE_EXTENSION) {
    form_set_error('git_username', t('The requested username is reserved, please choose a different username.'));
  }
}

/**
 * Perform arbitration to determine if the user should be allocated the
 * all-important 'Git user' role.
 */
function drupalorg_git_gateway_user_form_submit($form, &$form_state) {
  $edit = &$form_state['values'];
  $account = $form_state['account'];

  // If editing your own access, and have not already “consented,”
  // automatically consent. This was previously used to consent to ToS, which
  // is now done by GitLab. git_consent is now a flag for having set up Git
  // access.
  if ($account->uid == $GLOBALS['user']->uid && empty($account->git_consent)) {
    $edit['git_consent'] = TRUE;
  }
  if (isset($edit['git_consent']) && ($edit['git_consent'] != $account->git_consent)) {
    $id = db_insert('drupalorg_git_consent_log')
    ->fields(array(
      'uid' => $account->uid,
      'timestamp' => REQUEST_TIME,
      'action' => $edit['git_consent'],
    ))
    ->execute();
  }

  // Munge edited values into place on the account for unified checking.
  $fields = array();
  $dirty = FALSE;
  foreach (array('git_disabled', 'git_username', 'git_consent', 'git_vetted') as $field) {
    if (isset($edit[$field]) && $account->$field != $edit[$field]) {
      $fields[$field] = $edit[$field];
      $dirty = TRUE;
    }
  }
  if ($dirty) {
    user_save($account, $fields);
    if (empty($_REQUEST['destination'])) {
      drupal_set_message(t('Saved DrupalCode access.'));
    }
  }
}

/**
 * Handle administrative aspects of the Git user form - enabling/disabling the
 * account, and adding the vetted user status.
 */
function drupalorg_git_gateway_admin_user_form_submit($form, &$form_state) {
  $edit = &$form_state['values'];
  $account = $form_state['account'];
  global $user;

  // Admin has changed the enabled/disabled state of this git account. Log the change.
  if (isset($edit['git_disabled']) && ($edit['git_disabled'] != $account->git_disabled)) {
    $record = array(
      'uid' => $account->uid,
      'admin_uid' => $user->uid,
      'timestamp' => REQUEST_TIME,
      'action' => $edit['git_disabled'],
      'email' => '',
    );
    if ($edit['git_disabled'] == 1) {
      $body = t($edit['git_disabled_msg'], array('@user_name' => $account->name, '@admin_name' => $user->name));
      drupal_mail('drupalorg_git_gateway', 'disabled', $account->mail, user_preferred_language($account), array('body' => $body));
      $record['email'] = $body;
    }
    $id = db_insert('drupalorg_git_disable_log')
      ->fields($record)
      ->execute();
  }
}
